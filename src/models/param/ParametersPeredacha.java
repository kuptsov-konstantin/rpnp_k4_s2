package models.param;

public class ParametersPeredacha {
    String Email;
    String UserName;
    String PasshashMySQL;
    boolean isAutorise = false;
    private String ApplicationUserID;

    public ParametersPeredacha() {
    }

    public ParametersPeredacha(String email, String userName, String passhashMySQL, String applicationUserID) {
        this.Email = email;
        this.UserName = userName;
        this.PasshashMySQL = passhashMySQL;
        this.ApplicationUserID = applicationUserID;
    }

    public boolean isAutorise() {
        return isAutorise;
    }

    public void setAutorise(boolean autorise) {
        isAutorise = autorise;
    }

    public String getPasshashMySQL() {
        return PasshashMySQL;
    }

    public void setPasshashMySQL(String passhashMySQL) {
        PasshashMySQL = passhashMySQL;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public SaveParameters getSaveParameters() {
        return new SaveParameters(this.getApplicationUserID(), this.getPasshashMySQL());
    }

    public String getApplicationUserID() {
        return ApplicationUserID;
    }

    /* public UserTemp.UserInfo getUserTemp(){
         return new UserTemp.UserInfo();
     }*/
    public void setApplicationUserID(String applicationUserID) {
        this.ApplicationUserID = applicationUserID;
    }
}
