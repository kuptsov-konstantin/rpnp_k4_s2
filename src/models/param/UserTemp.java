package models.param;

public class UserTemp {
    //private static String file_xml = "temp.ini";
    //   private static Properties props = new Properties();
    // private static FileInputStream fis = null;

    static {
       /* try {
            File file = new File(file_xml);
            file.createNewFile();
            fis =   new FileInputStream(file);

            props.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public static void close() {
       /* if (null != fis)
        {
            try
            {
                fis.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }*/
    }

    public static String getApplicationUserID() {
        return System.getProperty("ApplicationUserID");
    }

    public static String getPasshashMySQL() {
        return System.getProperty("PasshashMySQL");
    }

    public static String getEmail() {
        return System.getProperty("Email");
    }

    public static String getUserName() {
        return System.getProperty("UserName");
    }

    public static boolean userTempSave(UserInfo pp) {
        //XMLEncoder e = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(file_xml)));

        System.setProperty("ApplicationUserID", pp.getApplicationUserID());
        System.setProperty("PasshashMySQL", pp.getPasshashMySQL());
        System.setProperty("Email", pp.getEmail());
        System.setProperty("UserName", pp.getUserName());
//        close();
        // e.writeObject(pp.getSaveParameters());
        //  e.close();
        return true;
    }

    public static boolean userTempSave(ParametersPeredacha pp) {
        userTempSave(new UserTemp.UserInfo(pp.getApplicationUserID(), pp.getPasshashMySQL(), pp.getEmail(), pp.getUserName()));
        return true;
    }

    public static boolean userTempDelete() {
       /* File file = new File(file_xml);
        if (file.exists()) {
            return file.delete();
        } else {
            return false;
        }*/
        return false;
    }

    public static class UserInfo extends ParametersPeredacha {
        public UserInfo(String applicationUserId, String hashpass, String eMail, String userName) {
            super(eMail, userName, hashpass, applicationUserId);
        }
    }
}
