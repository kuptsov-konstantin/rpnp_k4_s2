package models.param;

import models.JDBCSQL;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;

public class ParametersForVhod {
    private static String file_xml = "param.xml";


    public static ParametersPeredacha loginRead() {
        try {
            XMLDecoder xd = new XMLDecoder(new BufferedInputStream(new FileInputStream(file_xml)));
            ParametersPeredacha sp = (ParametersPeredacha) xd.readObject();
            return JDBCSQL.loging(sp.getSaveParameters());
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean loginSave(ParametersPeredacha pp) {
        try {
            XMLEncoder e = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(file_xml)));
            e.writeObject(pp);
            e.close();
        } catch (FileNotFoundException e) {
            return false;
        }
        return true;
    }

    public static boolean loginOut() {
        File file = new File(file_xml);
        if (file.exists()) {
            return file.delete();
        } else {
            return false;
        }
    }
}
