package models.param;

public class SaveParameters {
    String ApplicationUser;
    String hashpass;

    public SaveParameters() {
    }

    public SaveParameters(String applicationUser, String hashpass) {
        this.ApplicationUser = applicationUser;
        this.hashpass = hashpass;
    }

    public String getApplicationUser() {
        return ApplicationUser;
    }

    public void setApplicationUser(String applicationUser) {
        this.ApplicationUser = applicationUser;
    }

    public String getHashpass() {
        return hashpass;
    }

    public void setHashpass(String hashpass) {
        this.hashpass = hashpass;
    }
}
