package models;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import models.param.ParametersForVhod;
import models.param.ParametersPeredacha;
import models.param.UserTemp;

import java.io.IOException;

public class LoginController extends Stage {
    @FXML
    public GridPane loginwindowtarget;
    @FXML
    public CheckBox cheksavetarget;
    @FXML
    private Text actiontarget;
    @FXML
    private PasswordField passwordtarget;
    @FXML
    private TextField logintarget;

    public LoginController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/Login.fxml"));
        fxmlLoader.setController(this);
        try {
            Parent parent = (Parent) fxmlLoader.load();
            Scene scene = new Scene(parent, 300, 275);
            this.setScene(scene);
            this.setTitle("Вход в систему");
            // this.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) throws IOException {
        ParametersPeredacha pp = JDBCSQL.loging(logintarget.getText(), passwordtarget.getText());
        if (pp != null) {
            UserTemp.userTempSave(pp);
            if (cheksavetarget.isSelected() == true) {
                ParametersForVhod.loginSave(pp);
            }
            MainWindowController mwc = new MainWindowController();
            mwc.show();
            this.hide();


            /*FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../GUI/MainWindow.fxml"));
            Parent root = (Parent)fxmlLoader.load();
            MainWindowController controller = fxmlLoader.<MainWindowController>getController();
            controller.setParametersPeredacha(pp);
            controller._init();
            Stage primaryStage = new Stage();
            primaryStage.setTitle("Главное окно");
            primaryStage.setScene(new Scene(root, 400,500));
            primaryStage.show();*/
        } else {
            actiontarget.setText("Не верный логин/пароль");
        }
    }
}