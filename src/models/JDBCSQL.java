package models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import models.administration.firm.Firm;
import models.administration.firm.FirmsList;
import models.administration.firm.department.Department;
import models.administration.firm.projects.Project;
import models.administration.user.User;
import models.employees.Employee;
import models.employees.EmployeeTable;
import models.employees.People;
import models.param.ParametersPeredacha;
import models.param.SaveParameters;
import models.param.UserTemp;

import java.sql.*;
import java.time.LocalDate;
import java.util.Base64;

import static models.JDBCSQL.JDBCUser.getEmployeeFirm;

/**
 * Пулл подключения
 */
public final class JDBCSQL {

    private static final String userName = "admin_sql_infos";
    private static final String password = "ZXCasdqwe123";
    private static final String databaseName = "db_infos";
    private static  Statement stmt = null;

    static {

        Class clazz = null;
        try {
            clazz = Class.forName("com.mysql.jdbc.Driver");
            Driver driver = (Driver) (clazz.newInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private java.sql.Connection con = null;

    // Конструктор
    public JDBCSQL() {

    }

    /**
     * @return получение строки подключения
     */
    private static String getConnectionUrl() {
        return "jdbc:mysql://localhost:3306/" + databaseName + "?useSSL=false&noAccessToProcedureBodies=true";
    }

    /**
     * @return получение соединения
     * @throws SQLException
     */
    private static java.sql.Connection getConnection() throws SQLException {
        return java.sql.DriverManager.getConnection(getConnectionUrl(), userName, password);
    }

    /**
     * @param Email    почта
     * @param UserName логин
     * @param pass     пароль
     * @return Хэш пароля + логина и почты
     */
    public static String getAutoriseHash(String Email, String UserName, String pass) {
        String all = Email + UserName + pass;
        return Base64.getEncoder().encodeToString(all.getBytes());
    }

    /**
     * @param applicationUserID id пользователя из aspnetusers
     * @return список фирм в которых состоит пользователь
     */
    public static ObservableList<Firm> getPersonFirms(String applicationUserID) {
        try {
            java.sql.Connection con = getConnection();
            ObservableList<Firm> ols = FXCollections.observableArrayList();
            if (con != null) {
                stmt = con.createStatement();
                String sql_ = "SELECT firms.Name, firms.FirmID " +
                        "from firms, aspnetusers, employeefirms " +
                        "where aspnetusers.Id = \"" + applicationUserID + "\" and employeefirms.Employee_ApplicationUserId = aspnetusers.Id and employeefirms.Firm_FirmID = firms.FirmID";
                ResultSet rs = stmt.executeQuery(sql_);
                while (rs.next()) {
                    ols.add(new Firm(rs.getString("Name"), rs.getInt("FirmID")));
                }
                closeConnection(con);
                return ols;
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    /**
     * @param MasFirm список фирм
     * @return список сотрудников этих фирм
     */
    public static ObservableList<EmployeeTable> getEmployees(FirmsList MasFirm) {
        ObservableList<EmployeeTable> olet = FXCollections.observableArrayList();
        for (Firm ind : MasFirm.getNamesOrg()) {
            ObservableList<EmployeeTable> employees = getEmployees(ind.getIDFirm());
            if (employees != null) {
                olet.addAll(employees);
            }
        }
        return olet;
    }
    /**
     * @param MasIdFirm список фирм
     * @return список сотрудников этих фирм
     */
    public static ObservableList<EmployeeTable> getEmployees(ObservableList<Integer> MasIdFirm) {
        ObservableList<EmployeeTable> olet = FXCollections.observableArrayList();
        for (Integer ind : MasIdFirm) {
            ObservableList<EmployeeTable> employees = getEmployees(ind);
            if (employees != null) {
                olet.addAll(employees);
            }
        }
        return olet;
    }

    /**
     * @param idFirm id фирмы
     * @return список сотрудников
     */
    public static ObservableList<EmployeeTable> getEmployees(int idFirm) {
        try {
            java.sql.Connection con = getConnection();
            ObservableList<EmployeeTable> ols = FXCollections.observableArrayList();
            if (con != null) {
                stmt = con.createStatement();
                String sql_ = "SELECT people.BirthData, people.Family_name, people.Name, people.Middle_name, employeefirms.ApplicationUserId_Id, firms.Name, firms.FirmID " +
                        "FROM  people, firms, employeefirms, employees " +
                        "WHERE " +
                        "    people.IDPerson = employees.Person_IDPerson " +
                        "        AND employees.ApplicationUserId = employeefirms.Employee_ApplicationUserId " +
                        "        AND employeefirms.Firm_FirmID = firms.FirmID and firms.FirmID =" + idFirm;
                //"SELECT firms.Name, firms.FirmID from firms, aspnetusers, employeefirms where aspnetusers.Id = \"" + applicationUserID + "\" and employeefirms.Employee_ApplicationUserId = aspnetusers.Id and employeefirms.Firm_FirmID = " + idFirm;

                ResultSet rs = stmt.executeQuery(sql_);
                while (rs.next()) {
                    ols.add(
                            new EmployeeTable(
                                    rs.getDate("BirthData").toString(),
                                    null, rs.getString("firms.Name"),
                                    rs.getString("people.Family_name") + " " + rs.getString("people.Name") + " " + rs.getString("people.Middle_name"), null, rs.getString("ApplicationUserId_Id")));
                }
                closeConnection(con);
                return ols;
            } else
                System.out.println("Error: No active Connection");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Авторизация пользователя
     * @param sp параметр {@link SaveParameters}
     * @return передает полученные значения {@link ParametersPeredacha}
     */
    public static ParametersPeredacha loging(SaveParameters sp) {
        try {
            //   email = "motorolik93@mail.ru";
            java.sql.Connection con = getConnection();
            if (con != null) {
                ParametersPeredacha pp = new ParametersPeredacha();
                stmt = con.createStatement();
                String sql_ = "SELECT aspnetusers.Email, aspnetusers.UserName, mysqlhashpass.PasshashMySQL, aspnetusers.Id " +
                        "FROM aspnetusers, mysqlhashpass " +
                        "WHERE aspnetusers.Id = mysqlhashpass.ID AND aspnetusers.Id = \"" +
                        sp.getApplicationUser() + "\"";
                ResultSet rs = stmt.executeQuery(sql_);
                while (rs.next()) {
                    pp.setEmail(rs.getString("Email"));
                    pp.setUserName(rs.getString("UserName"));
                    pp.setPasshashMySQL(rs.getString("PasshashMySQL"));
                    pp.setApplicationUserID(rs.getString("Id"));
                    if (pp.getPasshashMySQL().compareTo(sp.getHashpass()) == 0) {
                        pp.setAutorise(true);
                    } else {
                        pp.setAutorise(false);
                    }
                }
                rs.close();
                closeConnection(con);
                return pp;
            } else
                System.out.println("Error: No active Connection");
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    /**
     * Производит авторизацию в программе.
     *
     * @param email E-Mail(логин пользователя)
     * @param pass  Пароль
     * @return Значения true или false, соответственно пройдена или нет
     */
    public static ParametersPeredacha loging(String email, String pass) {
        try {
            //   email = "motorolik93@mail.ru";
            java.sql.Connection con = getConnection();
            if (con != null) {
                ParametersPeredacha pp = new ParametersPeredacha();
                stmt = con.createStatement();
                String sql_ = "SELECT aspnetusers.Email, aspnetusers.UserName, mysqlhashpass.PasshashMySQL, aspnetusers.Id " +
                        "FROM aspnetusers, mysqlhashpass " +
                        "WHERE aspnetusers.Id = mysqlhashpass.ID AND Email = \"" + email + "\"";
                ResultSet rs = stmt.executeQuery(sql_);
                while (rs.next()) {
                    pp.setEmail(rs.getString("Email"));
                    pp.setUserName(rs.getString("UserName"));
                    pp.setPasshashMySQL(rs.getString("PasshashMySQL"));
                    pp.setApplicationUserID(rs.getString("Id"));
                    String hashpass = getAutoriseHash(pp.getEmail(), pp.getUserName(), pass);
                    if (pp.getPasshashMySQL().compareTo(hashpass) == 0) {
                        pp.setAutorise(true);
                    } else {
                        pp.setAutorise(false);
                    }
                }
                rs.close();
                closeConnection(con);
                return pp;
            } else
                System.out.println("Error: No active Connection");
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    private static void closeConnection(java.sql.Connection con) {
        try {
            if (con != null)
                con.close();
            con = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Выполнение не возвращаемого sql запроса
     * @param sql запрос
     * @return результат удачно/нет
     */
    protected static boolean execSQL(String sql) {
        try {
            java.sql.Connection con = getConnection();
            if (con != null) {
                stmt = con.createStatement();
                boolean rs = stmt.execute(sql);
                closeConnection(con);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    /**
     * Пул работы с отделами
     */
    public static class JDBCDepartments {

        /**
         * @param idFirm id фирмы
         * @return список отделов {@link ObservableList<Department>}
         */
        public static ObservableList<Department> getDepartments(int idFirm) {
            try {
                java.sql.Connection con = getConnection();
                ObservableList<Department> ols = FXCollections.observableArrayList();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql_ = "select departments.Name, departments.DepartmentID, departments.Firm_FirmID, (select count(*) from departmentemployeefirms where departmentemployeefirms.Department_DepartmentID =  departments.DepartmentID) from departments WHere departments.Firm_FirmID = " + idFirm;
                    //"SELECT firms.Name, firms.FirmID from firms, aspnetusers, employeefirms where aspnetusers.Id = \"" + applicationUserID + "\" and employeefirms.Employee_ApplicationUserId = aspnetusers.Id and employeefirms.Firm_FirmID = " + idFirm;

                    ResultSet rs = stmt.executeQuery(sql_);
                    while (rs.next()) {
                        ols.add(new Department(rs.getString(1), rs.getInt(2), rs.getInt(3), rs.getInt(4)));
                    }
                    closeConnection(con);
                    return ols;
                } else
                    System.out.println("Error: No active Connection");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * @param mas список фирм
         * @return список отделов переданных фирм
         */
        public static ObservableList<Department> getDepartments(ObservableList<Integer> mas) {
            ObservableList<Department> olet = FXCollections.observableArrayList();
            for (Integer integer: mas) {
                ObservableList<Department> employees = getDepartments(integer);
                if (employees != null) {
                    olet.addAll(employees);
                }
            }
            return olet;
        }

        /**
         * Добавление отдела в фирму
         * @param text наименование отдела
         * @param idFirm id фирмы в которую надо добавить отдел
         * @return результат операции
         */
        public static boolean addDepartmetnToFirm(String text, int idFirm) {
            return execSQL("INSERT INTO departments (Name, Firm_FirmID) VALUES(\"" + text + "\"," + idFirm + ")");
        }

        /**
         * Удаление отдела из фирмы
         * @param idDepartment id отдела
         * @return результат операции
         */
        public static boolean delDepartment(int idDepartment) {
            ObservableList<Employee> departmentEmployee = getDepartmentEmployee(idDepartment);
            for (Employee emp : departmentEmployee) {
                int employeeFirm = getEmployeeFirm(emp.getAppUserId());
                delDepartmentEmployee(idDepartment, employeeFirm);
            }
            return execSQL("DELETE FROM departments WHERE DepartmentID = " + idDepartment);
        }

        /**
         * Удаление связи отдела - сотрудник
         * @param idDepartment id отдела
         * @param idEF id сотрудника
         * @return результат операции
         */
        public static boolean delDepartmentEmployee(int idDepartment, int idEF) {
            return execSQL("DELETE FROM departmentemployeefirms WHERE Department_DepartmentID = " + idDepartment + " and EmployeeFirm_Id = " + idEF);
        }

        /**
         * Список сотрудников отдела
         * @param idDep id отдела
         * @return список сотрудников
         */
        public static ObservableList<Employee> getDepartmentEmployee(int idDep) {
            try {
                java.sql.Connection con = getConnection();
                ObservableList<Employee> ols = FXCollections.observableArrayList();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql_ = "select people.Family_name, people.Name, people.Middle_name, employeefirms.isChief, employeefirms.ApplicationUserId_Id \n" +
                            "from people, employeefirms, departmentemployeefirms, employees\n" +
                            "where departmentemployeefirms.EmployeeFirm_Id = employeefirms.Id\n" +
                            "and employeefirms.Employee_ApplicationUserId = employees.ApplicationUserId\n" +
                            "and employees.Person_IDPerson = people.IDPerson and departmentemployeefirms.Department_DepartmentID = " + idDep;
                    //"SELECT firms.Name, firms.FirmID from firms, aspnetusers, employeefirms where aspnetusers.Id = \"" + applicationUserID + "\" and employeefirms.Employee_ApplicationUserId = aspnetusers.Id and employeefirms.Firm_FirmID = " + idFirm;

                    ResultSet rs = stmt.executeQuery(sql_);
                    while (rs.next()) {
                        ols.add(new Employee(rs.getString(1), rs.getString(2), rs.getString(3), rs.getBoolean(4), rs.getString(5)));
                    }
                    closeConnection(con);
                    return ols;
                } else
                    System.out.println("Error: No active Connection");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Обновление информации об отделе
         * @param idDep id отдела
         * @param nameDep название отдела
         * @param employeesDep список сотрудников
         * @return результат операции
         */
        public static boolean setDepartment(int idDep, String nameDep, ObservableList<Employee> employeesDep) {
            boolean b = execSQL("UPDATE departments SET Name ='" + nameDep + "' WHERE DepartmentID = " + idDep);
            //  execSQL("SELECT Department_DepartmentID, EmployeeFirm_Id FROM departmentemployeefirms WHERE Department_DepartmentID = " + idDep + " and EmployeeFirm_Id = employeefirms.Id and e")
            ObservableList<Employee> departmentEmployee = getDepartmentEmployee(idDep);
            for (Employee demp : departmentEmployee) {
                boolean find = false;
                for (Employee emp : employeesDep) {
                    if (emp.getAppUserId().compareTo(demp.getAppUserId()) == 0) {
                        find = true;
                    }
                }
                if (find == false) {
                    int employeeFirm = getEmployeeFirm(demp.getAppUserId());
                    delDepartmentEmployee(idDep, employeeFirm);
                }
            }
            departmentEmployee = getDepartmentEmployee(idDep);

            for (Employee emp : employeesDep) {
                boolean find = false;
                for (Employee demp : departmentEmployee) {
                    if (emp.getAppUserId().compareTo(demp.getAppUserId()) == 0) {
                        find = true;
                    }
                }
                if (find == false) {
                    int employeeFirm = getEmployeeFirm(emp.getAppUserId());
                    if (employeeFirm != -1) {
                        boolean b1 = execSQL("INSERT  INTO  departmentemployeefirms (Department_DepartmentID, EmployeeFirm_Id) VALUE (" + idDep + ", " + employeeFirm + ")");
                    }
                }
            }

            return b;
        }
    }

    /**
     * Пул работы с фирмами
     */
    public static class JDBCFirms {
        /**
         * Получение списка фирм в которых состоит пользователь
         * @param applicationUserID applicationUserID пользователя
         * @param withDel true показывает удаленные, false действующие
         * @return список фирм
         */
        public static FirmsList getFirms(String applicationUserID, boolean withDel) {
            try {
                FirmsList fl = new FirmsList(applicationUserID);
                java.sql.Connection con = getConnection();
                if (con != null) {
                    ParametersPeredacha pp = new ParametersPeredacha();
                    stmt = con.createStatement();
                    String sql = "SELECT firms.Name, firms.FirmID FROM firms, employeefirms " +
                            "WHERE FirmID = employeefirms.Firm_FirmID AND " +
                            "employeefirms.ApplicationUserId_Id = \"" + applicationUserID + "\" AND Del = " + withDel;

                    ResultSet rs = stmt.executeQuery(sql);
                    while (rs.next()) {
                        fl.addNewFirm(rs.getString("Name"), rs.getInt("FirmID"));
                    }
                    closeConnection(con);
                    return fl;
                }
            } catch (Exception e) {
                return null;
            }
            return null;
        }


        /**
         * Добавление новой фирмы в базу
         * @param text название фирмы
         * @return результат операции
         */
        public static boolean addNewFirm(String text) {
            return execSQL("call addFirm(\"" + UserTemp.getApplicationUserID() + "\",\"" + text + "\")");
        }

        /**
         * Удаление фирмы
         * @param idFirm id фирмы
         * @return результат операции
         */
        public static boolean delFirm(int idFirm) {
            return hideFirm(idFirm, true);
        }
        /**
         * Восстановление фирмы
         * @param idFirm id фирмы
         * @return результат операции
         */
        public static boolean returnFirm(int idFirm) {
            return hideFirm(idFirm, false);
        }
        /**
         * Скрытие/открытие фирмы
         * @param idFirm id фирмы
         * @return результат операции
         */
        private static boolean hideFirm(int idFirm, boolean isHide) {
            return execSQL("UPDATE firms SET Del =" + isHide + " WHERE FirmID = " + idFirm);
        }


        /**
         * Обновление наименования фирмы
         * @param idFirm id фирмы
         * @param text новое имя
         * @return результат операции
         */
        public static boolean updateName(int idFirm, String text) {
            return execSQL("UPDATE firms SET Name = \"" + text + "\" WHERE FirmID = " + idFirm);
        }

        /**
         * Получения списка сотрудников фирмы
         * @param idFirm id фирмы
         * @return возвращение списка сотрудников
         */
        public static ObservableList<Employee> getEmployees(int idFirm) {
            try {
                java.sql.Connection con = getConnection();
                ObservableList<Employee> ols = FXCollections.observableArrayList();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql_ = "select employeefirms.isChief, employeefirms.ApplicationUserId_Id, people.Name, people.Family_name, people.Middle_name " +
                            "from employeefirms, people, employees\n" +
                            "where employeefirms.Firm_FirmID =" + idFirm + " and employeefirms.Employee_ApplicationUserId = employees.ApplicationUserId and employees.Person_IDPerson = people.IDPerson ";
                    //"SELECT firms.Name, firms.FirmID from firms, aspnetusers, employeefirms where aspnetusers.Id = \"" + applicationUserID + "\" and employeefirms.Employee_ApplicationUserId = aspnetusers.Id and employeefirms.Firm_FirmID = " + idFirm;

                    ResultSet rs = stmt.executeQuery(sql_);
                    while (rs.next()) {
                        ols.add(new Employee(rs.getString("Family_name"), rs.getString("Name"), rs.getString("Middle_name"), rs.getBoolean("isChief"), rs.getString("ApplicationUserId_Id")));
                    }
                    closeConnection(con);
                    return ols;
                } else
                    System.out.println("Error: No active Connection");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Обноволение информации о директоре фирмы
         * @param idFirm id фирмы
         * @param appUserId id пользователя
         * @param b выставление значения - да/нет
         * @return
         */
        public static boolean updateChief(int idFirm, String appUserId, boolean b) {
            return execSQL("UPDATE employeefirms SET isChief = " + b + " WHERE employeefirms.Firm_FirmID = " + idFirm + " AND employeefirms.ApplicationUserId_Id = \"" + appUserId + "\"");
        }

        /**
         * Формирование списка всех пользователей указанных фирм
         * @param firmsList список фирм
         * @return список пользователей
         */
        public static ObservableList<Employee> getEmployees(FirmsList firmsList) {
            ObservableList<Employee> employeeObservableList = FXCollections.observableArrayList();
            for (Firm firm : firmsList.getNamesOrg()) {
                employeeObservableList.addAll(getEmployees(firm.getIDFirm()));
            }
            return employeeObservableList;
        }
    }


    /**
     * Пул поиска
     */
    public static class JDBCFind {
        public static boolean Firm(String text) {
            try {
                boolean find = false;
                java.sql.Connection con = getConnection();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql = "SELECT firms.Name FROM firms " +
                            "WHERE firms.Name = \"" + text + "\"";
                    ResultSet rs = stmt.executeQuery(sql);
                    while (rs.next()) {
                        if (rs.getString("Name").toLowerCase().compareTo(text.toLowerCase()) == 0) {
                            find = true;
                        }
                    }
                    closeConnection(con);
                    return find;
                }

            } catch (Exception e) {
                return false;
            }
            return false;
        }


        public static boolean EMail(String text) {
            try {
                boolean find = false;
                java.sql.Connection con = getConnection();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql = "SELECT  Email FROM aspnetusers " +
                            "WHERE Email = \"" + text + "\"";
                    ResultSet rs = stmt.executeQuery(sql);
                    while (rs.next()) {
                        if (rs.getString("Email").toLowerCase().compareTo(text.toLowerCase()) == 0) {
                            find = true;
                        }
                    }
                    closeConnection(con);
                    return find;
                }

            } catch (Exception e) {
                return false;
            }
            return false;
        }

        public static boolean Login(String text) {
            try {
                boolean find = false;
                java.sql.Connection con = getConnection();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql = "SELECT UserName FROM aspnetusers " +
                            "WHERE UserName = \"" + text + "\"";
                    ResultSet rs = stmt.executeQuery(sql);
                    while (rs.next()) {
                        if (rs.getString("UserName").toLowerCase().compareTo(text.toLowerCase()) == 0) {
                            find = true;
                        }
                    }
                    closeConnection(con);
                    return find;
                }

            } catch (Exception e) {
                return false;
            }
            return false;
        }
    }

    /**
     * Пул работы с пользователями
     */
    public static class JDBCUser {

        /**
         * Получение EmployeeFirm ID
         * @param appUserId aspnet ApplicationUserID
         * @return результат операции
         */
        public static int getEmployeeFirm(String appUserId) {
            try {
                java.sql.Connection con = getConnection();
                ObservableList<Employee> ols = FXCollections.observableArrayList();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql_ = "SELECT  employeefirms.Id FROM  employeefirms WHERE employeefirms.ApplicationUserId_Id = '" + appUserId + "'";
                    //"SELECT firms.Name, firms.FirmID from firms, aspnetusers, employeefirms where aspnetusers.Id = \"" + applicationUserID + "\" and employeefirms.Employee_ApplicationUserId = aspnetusers.Id and employeefirms.Firm_FirmID = " + idFirm;

                    ResultSet rs = stmt.executeQuery(sql_);
                    while (rs.next()) {
                        return rs.getInt(1);
                        //ols.add(new Employee(rs.getString(1), rs.getString(2), rs.getString(3), rs.getBoolean(4), rs.getString(5)));
                    }
                    closeConnection(con);
                    return -1;
                } else
                    System.out.println("Error: No active Connection");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return -1;
        }

        /**
         * Регистрация пользователя
         * @param username логин
         * @param passTargetText пароль
         * @param emailTargetText EMail
         * @param people информация о человеке
         * @param firms информация о фирмах в которых будет зарегистрирован
         * @return результат операции - true/false
         */
        public static boolean register(String username, String passTargetText, String emailTargetText, People people, ObservableList<Firm> firms) {
            try {
                java.sql.Connection con = getConnection();
                if (con != null) {
                    String authoriseHash = getAutoriseHash(emailTargetText, username, passTargetText);
                    CallableStatement cs = con.prepareCall("{ call addUser(?, ?, ?, ?, ?, ?, ?, ?) }");
                    cs.setString(1, username);
                    cs.setString(2, emailTargetText);
                    cs.setString(3, authoriseHash);
                    cs.setString(4, people.getFamily());
                    cs.setString(5, people.getName());
                    cs.setString(6, people.getMiddle());
                    cs.setDate(7, Date.valueOf(people.getBirthDateTargetValue()));
                    cs.registerOutParameter(8, Types.VARCHAR);
                    ResultSet rs = cs.executeQuery();
                    String returnValue = cs.getString(8);
//in username varchar(256), in email varchar(256), in passhash varchar(256), in _family longtext, in _name longtext, in _middle longtext
                    //    boolean bSQL = execSQL("call CreateNewUser(\"" + username + "\", \"" + emailTargetText + "\", \"" + authoriseHash + "\", \"" + people.getFamily() + "\", \"" + people.getName() + "\", \"" + people.getMiddle() + "\", " + Date.valueOf(people.getBirthDateTargetValue()) + ")");
                    for (Firm firm : firms) {
                        boolean bSQL = execSQL("INSERT INTO db_infos.employeefirms (Employee_ApplicationUserId, ApplicationUserId_Id, Firm_FirmID) VALUES ('" + returnValue + "', " + " '" + returnValue + "', " + firm.getIDFirm() + ")");
                    }
                    return true;
                }

            } catch (Exception e) {
                return false;
            }
            return false;
        }


        /**
         * Получение информации опльзователе {@link User}
         * @param appUserID aspnet ApplicationUserID
         * @return {@link User}
         */
        public static User getPerson(String appUserID) {
            try {
                java.sql.Connection con = getConnection();
                User user = new User();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql_ = "SELECT people.Family_name,people.Name,people.Middle_name,people.BirthData,aspnetusers.Email,aspnetusers.UserName " +
                            "FROM people,aspnetusers,employees " +
                            "WHERE people.IDPerson = employees.Person_IDPerson and employees.ApplicationUserId = aspnetusers.Id AND aspnetusers.Id = '" + appUserID + "'";
                    ResultSet rs = stmt.executeQuery(sql_);
                    while (rs.next()) {
                        user = new User(rs.getString("Family_name"), rs.getString("Name"), rs.getString("Middle_name"));
                        user.setBirthDateTargetValue(rs.getDate("BirthData"));
                        user.setEmailP(rs.getString("Email"));
                        user.setLogin(rs.getString("UserName"));
                        //    ols.add(new Employee(rs.getString("Family_name"), rs.getString("Name"), rs.getString("Middle_name"), rs.getBoolean("isChief"), rs.getString("ApplicationUserId_Id")));
                    }
                    closeConnection(con);
                    FirmsList firms = JDBCFirms.getFirms(appUserID, false);
                    user.setIdFirms(firms);


                    return user;
                } else
                    System.out.println("Error: No active Connection");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        public static boolean setBirthDay(String appUserID, LocalDate _value) {
            execSQL("UPDATE people SET  BirthData = " + _value + " WHERE people.IDPerson = employees.Person_IDPerson AND employees.ApplicationUserId = '" + appUserID + "'");
            return false;
        }

        public static boolean setFamily(String appUserID, String text) {
            execSQL("UPDATE people SET  Family_name = " + text + " WHERE people.IDPerson = employees.Person_IDPerson AND employees.ApplicationUserId = '" + appUserID + "'");
            return false;

        }

        public static boolean setName(String appUserID, String text) {
            execSQL("UPDATE people SET  Name = " + text + " WHERE people.IDPerson = employees.Person_IDPerson AND employees.ApplicationUserId = '" + appUserID + "'");
            return false;
        }

        public static boolean setMiddleName(String appUserID, String text) {
            execSQL("UPDATE people SET  Middle_name = " + text + " WHERE people.IDPerson = employees.Person_IDPerson AND employees.ApplicationUserId = '" + appUserID + "'");
            return false;
        }

        public static boolean setLogin(String appUserID, String text) {
            execSQL("UPDATE aspnetusers SET  UserName = " + text + " WHERE employees.ApplicationUserId = '" + appUserID + "'");
            return false;
        }

        public static boolean setEMail(String appUserID, String text) {
            execSQL("UPDATE aspnetusers SET  Email = " + text + " WHERE employees.ApplicationUserId = '" + appUserID + "'");
            return false;
        }

        public static boolean setPass(String appUserID, String login, String email, String pass) {

            execSQL("UPDATE mysqlhashpass SET  PasshashMySQL = " + JDBCSQL.getAutoriseHash(email, login, pass) + " WHERE ID = '" + appUserID + "'");
            return false;
        }

        public static void setFirms(String appUserID, ObservableList<Firm> firms) {
            for (Firm firm : firms) {
                boolean bSQL = execSQL("INSERT INTO db_infos.employeefirms (Employee_ApplicationUserId, ApplicationUserId_Id, Firm_FirmID) VALUES ('" + appUserID + "', " + " '" + appUserID + "', " + firm.getIDFirm() + ")");
            }
        }

        public static boolean delUser(String appUserID) {
            return execSQL("CALL delUser('" + appUserID + "')");
        }
    }

    /**
     * Пул работы с проектами
     */
    public static class JDBCProject {
        /**
         * Список доступных проектов пользователю
         * @param applicationUserID ID пользователя
         * @param idFirm id фирмы
         * @return список доступных проектов пользователю
         */
        public static ObservableList<Project> getUserProjects(String applicationUserID, int idFirm) {
            try {
                java.sql.Connection con = getConnection();
                ObservableList<Project> ols = FXCollections.observableArrayList();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql_ = "";
                    if (idFirm >= 0) {
                        sql_ = "select projects.НазваниеПроекта, firms.Name, projects.ProjectID, firms.FirmID, projects.PlannedFinishDate " +
                                "from projects, firms, employeefirms, projectemployeefirms " +
                                "where projects.ProjectID = projectemployeefirms.Project_ProjectID " +
                                "and projectemployeefirms.EmployeeFirm_Id = employeefirms.Id " +
                                "and employeefirms.ApplicationUserId_Id = '" + applicationUserID + "' " +
                                "and employeefirms.Firm_FirmID = " + idFirm + " AND firms.FirmID = " + idFirm;
                        //"SELECT firms.Name, firms.FirmID from firms, aspnetusers, employeefirms where aspnetusers.Id = \"" + applicationUserID + "\" and employeefirms.Employee_ApplicationUserId = aspnetusers.Id and employeefirms.Firm_FirmID = " + idFirm;
                    } else {
                        sql_ = "select projects.НазваниеПроекта, firms.Name, projects.ProjectID, firms.FirmID, projects.PlannedFinishDate " +
                                "from projects, firms, employeefirms, projectemployeefirms " +
                                "where projects.ProjectID = projectemployeefirms.Project_ProjectID " +
                                "and projectemployeefirms.EmployeeFirm_Id = employeefirms.Id " +
                                "and employeefirms.ApplicationUserId_Id = '" + applicationUserID + "' " +
                                "and employeefirms.Firm_FirmID = firms.FirmID ";
                    }
                    ResultSet rs = stmt.executeQuery(sql_);
                    while (rs.next()) {
                        ols.add(new Project(rs.getString("Name"), rs.getInt("FirmID"), rs.getString("НазваниеПроекта"), rs.getInt("ProjectID"), rs.getDate("PlannedFinishDate")));
                    }
                    closeConnection(con);
                    return ols;
                } else
                    System.out.println("Error: No active Connection");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Список всех проектов фирмы
         * @param idFirm id фирмы
         * @return список проектов {@link ObservableList<Project> }
         */
        public static ObservableList<Project> getProjectsFirm(Firm idFirm) {
            try {
                java.sql.Connection con = getConnection();
                ObservableList<Project> ols = FXCollections.observableArrayList();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql_ = "select projects.НазваниеПроекта, projects.ProjectID, projects.PlannedFinishDate " +
                            "FROM projects WHERE projects.Firm_FirmID = " + idFirm.getIDFirm();
                    ResultSet rs = stmt.executeQuery(sql_);
                    while (rs.next()) {
                        ols.add(new Project(idFirm.getNameFirm(), idFirm.getIDFirm(), rs.getString("НазваниеПроекта"), rs.getInt("ProjectID"), rs.getDate("PlannedFinishDate")));
                    }
                    closeConnection(con);
                    return ols;
                } else
                    System.out.println("Error: No active Connection");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Добавление в ProjectEmployeeFirms
         * @param projectID id проекта
         * @param appUserID id пользователя
         * @return результат операции
         */
        public static boolean addProjectEmployeeFirms(int projectID, String appUserID) {
            Connection con = null;
            try {
                con = getConnection();

                CallableStatement cs1 = con.prepareCall("{ call addProjectEmplyeeFirms(?, ?) }");
                cs1.setString(1, appUserID);
                cs1.setInt(2, projectID);
                ResultSet rs1 = cs1.executeQuery();
                cs1.cancel();
                closeConnection(con);
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }

        /**
         * Добавление проекта в базу
         * @param text название проекта
         * @param startDateValue дата начала проектирования
         * @param endvalue дата окончания проекта
         * @param firm ответственная фирма
         * @param employeeTables список задействованого персонала
         * @return результат действия
         */
        public static boolean addProject(String text, LocalDate startDateValue, LocalDate endvalue, Firm firm, ObservableList<EmployeeTable> employeeTables) {
            try {
                java.sql.Connection con = getConnection();
                if (con != null) {
                    CallableStatement cs = con.prepareCall("{ call addProject(?, ?, ?, ?, ?) }");
                    cs.setString(1, text);
                    cs.setDate(2, Date.valueOf(startDateValue));
                    cs.setDate(3, Date.valueOf(endvalue));
                    cs.setInt(4, firm.getIDFirm());
                    cs.registerOutParameter(5, Types.INTEGER);
                    ResultSet rs = cs.executeQuery();
                    int returnValue = cs.getInt(5);
                    cs.cancel();
                    closeConnection(con);
                    for (EmployeeTable employeeTable : employeeTables) {
                        addProjectEmployeeFirms(returnValue, employeeTable.getAppUserID());
                    }
                    return true;
                }

            } catch (Exception e) {
                return false;
            }
            return false;
        }

        public static Project getProject(int projectID) {
            try {
                java.sql.Connection con = getConnection();
                Project project = new Project();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql_ = "SELECT projects.НазваниеПроекта,projects.ProjectStartDate,projects.PlannedFinishDate, projects.Firm_FirmID " +
                            "FROM projects " +
                            "WHERE projects.ProjectID = " + projectID;
                    ResultSet rs = stmt.executeQuery(sql_);
                    while (rs.next()) {
                        project = new Project();
                        project.setEndDate(rs.getDate("PlannedFinishDate"));
                        project.setStartDate(rs.getDate("ProjectStartDate"));
                        project.setProjectName(rs.getString("НазваниеПроекта"));
                        project.setProjectID(projectID);
                        project.setFirmID(rs.getInt("Firm_FirmID"));
                        //    ols.add(new Employee(rs.getString("Family_name"), rs.getString("Name"), rs.getString("Middle_name"), rs.getBoolean("isChief"), rs.getString("ApplicationUserId_Id")));
                    }
                    closeConnection(con);
                    return project;
                } else
                    System.out.println("Error: No active Connection");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        public static ObservableList<Employee> getProjectEmployee(int projectID) {
            try {
                java.sql.Connection con = getConnection();
                ObservableList<Employee> employeeObservableList = FXCollections.observableArrayList();
                if (con != null) {
                    stmt = con.createStatement();
                    String sql_ = "SELECT people.Name, people.Family_name, people.Middle_name, employeefirms.isChief, employeefirms.ApplicationUserId_Id " +
                            "FROM projectemployeefirms, employeefirms, projects, people, employees " +
                            "WHERE projects.ProjectID = " + projectID +
                            " AND projectemployeefirms.Project_ProjectID = " + projectID +
                            " AND projectemployeefirms.EmployeeFirm_Id = employeefirms.Id " +
                            " AND projects.ProjectID = projectemployeefirms.Project_ProjectID AND people.IDPerson = employees.Person_IDPerson AND  employees.ApplicationUserId = employeefirms.ApplicationUserId_Id";
                    ResultSet rs = stmt.executeQuery(sql_);
                    while (rs.next()) {
                        employeeObservableList.add(new Employee(rs.getString("Family_name"), rs.getString("Name"), rs.getString("Middle_name"), rs.getBoolean("isChief"), rs.getString("ApplicationUserId_Id")));
                    }
                    closeConnection(con);
                    return employeeObservableList;
                } else
                    System.out.println("Error: No active Connection");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }


        public static boolean updateProject(int projectId, String namePrj, LocalDate startDate, LocalDate endDate, Firm firm) {
            boolean b = execSQL("UPDATE projects SET НазваниеПроекта = '" + namePrj + "',ProjectStartDate = " + startDate.toString() + " , PlannedFinishDate = " + endDate.toString() + ", Firm_FirmID = " + firm.getIDFirm() + " WHERE ProjectID = " + projectId);

           /* for (Employee employeeTable : employeeObservableList) {
                addProjectEmployeeFirms(projectId, employeeTable.getAppUserId());
            }*/
            return b;
        }

        public static boolean delProjectEmployeeFirms(int projectID, String appUserId) {
            Connection con = null;
            try {
                con = getConnection();
                CallableStatement cs1 = con.prepareCall("{ call delProjectEmplyeeFirms(?, ?) }");
                cs1.setString(1, appUserId);
                cs1.setInt(2, projectID);
                ResultSet rs1 = cs1.executeQuery();
                cs1.cancel();
                closeConnection(con);
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }

        }

        public static boolean delProject(int projectID) {
            ObservableList<Employee> projectEmployee = getProjectEmployee(projectID);
            for (Employee employee : projectEmployee) {
                delProjectEmployeeFirms(projectID, employee.getAppUserId());
            }
            return execSQL("DELETE FROM projects WHERE ProjectID = " + projectID);
        }
    }
}