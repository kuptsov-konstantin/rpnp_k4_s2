package models.administration.firm.projects;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class TestTable {
    private final StringProperty ProjectName;
    private final StringProperty Otv;
    private final StringProperty Deadline;
    private Project project;

    public TestTable() {
        this(null);
    }

    public TestTable(Project prj) {
        this.ProjectName = new SimpleStringProperty(prj.getProjectName());
        this.Otv = new SimpleStringProperty(prj.getOtv());
        this.Deadline = new SimpleStringProperty(prj.getEndDate().toString());
        project = prj;
    }

    public String getDeadline() {
        return Deadline.get();
    }

    public void setDeadline(String deadline) {
        this.Deadline.set(deadline);
    }

    public StringProperty deadlineProperty() {
        return Deadline;
    }

    public String getOtv() {
        return Otv.get();
    }

    public void setOtv(String otv) {
        this.Otv.set(otv);
    }

    public StringProperty otvProperty() {
        return Otv;
    }

    public String getProjectName() {
        return ProjectName.get();
    }

    public void setProjectName(String projectName) {
        this.ProjectName.set(projectName);
    }

    public StringProperty projectNameProperty() {
        return ProjectName;
    }

    public Project getProject() {
        return project;
    }
}
