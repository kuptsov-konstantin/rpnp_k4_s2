package models.administration.firm.projects;

import models.administration.firm.Firm;

import java.sql.Date;

public class Project {
    private int ProfectID;
    private Firm firm = new Firm();
    private String ProjectName;
    private Date EndDate;
    private Date StartDate;
    private String otv = "";
    // private String otv;

    public Project() {
    }

    public Project(String firmName, int firmID, String projectName, int profectID) {
        this.firm.setIDFirm(firmID);
        this.firm.setNameFirm(firmName);
        this.ProjectName = projectName;
        this.ProfectID = profectID;
    }

    public Project(String firmName, int firmID, String projectName, int profectID, Date _endDate) {
        this.EndDate = _endDate;
        this.firm.setIDFirm(firmID);
        this.firm.setNameFirm(firmName);
        this.ProjectName = projectName;
        this.ProfectID = profectID;
    }

    public Project(String firmName, int firmID, String projectName, int profectID, Date _endDate, Date _startDate) {
        this.EndDate = _endDate;
        this.StartDate = _startDate;
        this.firm.setIDFirm(firmID);
        this.firm.setNameFirm(firmName);
        this.ProjectName = projectName;
        this.ProfectID = profectID;
    }

    public int getFirmID() {
        return this.firm.getIDFirm();
    }

    public void setFirmID(int firmID) {
        this.firm.setIDFirm(firmID);
    }

    public int getProjectID() {
        return ProfectID;
    }

    public void setProjectID(int profectID) {
        this.ProfectID = profectID;
    }

    public String getFirmName() {
        return this.firm.getNameFirm();
    }

    public void setFirmName(String firmName) {
        this.firm.setNameFirm(firmName);
    }

    public String getProjectName() {
        return ProjectName;
    }

    public void setProjectName(String projectName) {
        this.ProjectName = projectName;
    }

    public Date getEndDate() {
        return this.EndDate;
    }

    public void setEndDate(Date deadline) {
        this.EndDate = deadline;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date startDate) {
        StartDate = startDate;
    }

    public String getOtv() {
        return otv;
    }
}
