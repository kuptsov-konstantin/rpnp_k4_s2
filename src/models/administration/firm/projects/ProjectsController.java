package models.administration.firm.projects;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import models.JDBCSQL;
import models.MainStage;
import models.administration.firm.Firm;
import models.administration.firm.project.ProjectAddController;
import models.administration.firm.project.ProjectEditController;
import models.param.UserTemp;

import java.net.URL;
import java.util.ResourceBundle;


public class ProjectsController extends MainStage implements Initializable {

    public Text projectnametarget;
    @FXML
    public ListView firmstarget;
    @FXML
    public TableColumn tablevProjectNameTarget;
    @FXML
    public TableColumn tablevOtvTarget;
    @FXML
    public TableColumn tablevDeadlineTarget;
    ObservableList<TestTable> olt = FXCollections.observableArrayList();
    ObservableList<Firm> olf = FXCollections.observableArrayList();
    ObservableList<Project> olp = FXCollections.observableArrayList();
    @FXML
    private TableView<TestTable> Tablev;

    public ProjectsController() {
        //super("/gui/Projects.fxml","Доступные проекты", 400, 500);
        super.loadFxml(getClass().getResource("/gui/administration/firm/Projects.fxml"), "Доступные проекты", 550, 500, this, ProjectsController.class);

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        olf = JDBCSQL.getPersonFirms(UserTemp.getApplicationUserID());
        // olf.add(0, new Firm("Все", -1));
        firmstarget.setItems(olf);
        firmstarget.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Firm>() {
                    public void changed(ObservableValue<? extends Firm> ov, Firm old_val, Firm new_val) {
                        projectnametarget.setText("Проекты фирмы " + new_val.getNameFirm());
                        olt.clear();
                        olp = JDBCSQL.JDBCProject.getProjectsFirm(new_val);
                        for (Project prj : olp) {
                            olt.add(new TestTable(prj));
                        }
                    }
                });
        firmstarget.getSelectionModel().select(0);
        Tablev.setItems(olt);


        ContextMenu menu = new ContextMenu();
        MenuItem izm_item = new MenuItem("Изменить");
        MenuItem del_item = new MenuItem("Удалить");
        // izm_item.setOnAction(event -> System.out.println("you've clicked the menu item"));
        izm_item.setOnAction(event -> {
            TestTable prj = Tablev.getSelectionModel().getSelectedItem();
            System.out.println(prj.toString());
            ProjectEditController uec = new ProjectEditController(prj.getProject());
            uec.show();
        });
        del_item.setOnAction(event -> {
            TestTable person = Tablev.getSelectionModel().getSelectedItem();
            JDBCSQL.JDBCProject.delProject(person.getProject().getProjectID());
            Tablev.getItems().remove(Tablev.getSelectionModel().getSelectedItem());
        });
        menu.getItems().addAll(izm_item, del_item);
        Tablev.setContextMenu(menu);
    }

    public void addProject(ActionEvent actionEvent) {
        ProjectAddController pad = new ProjectAddController();
        pad.show();
    }
}
