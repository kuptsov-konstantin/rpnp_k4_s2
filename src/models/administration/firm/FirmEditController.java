package models.administration.firm;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import jfx.messagebox.MessageBox;
import models.JDBCSQL;
import models.MainStage;
import models.employees.Employee;
import models.param.UserTemp;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Константин on 06.03.2016.
 */
public class FirmEditController extends MainStage implements Initializable {
    public Text createrTarget;
    public TextField nameFirmTarget;
    public Text actiontarget;
    public ComboBox listPersTarget;
    ObservableList<Employee> employees;
    private Firm firm;

    public FirmEditController() {
        // super("/gui/administration/firm/FirmEditor.fxml", "Редактирвоание фирм", 400, 500);
        super.loadFxml(
                getClass().getResource("/gui/administration/firm/FirmEdit.fxml"),
                "Изменение данных о фирме", 400, 500,
                this, FirmEditController.class);

    }

    public FirmEditController(Firm Firm_) {
        // super("/gui/administration/firm/FirmEditor.fxml", "Редактирвоание фирм", 400, 500);
        super.loadFxml(
                getClass().getResource("/gui/administration/firm/FirmEdit.fxml"),
                "Изменение данных о фирме", 400, 500,
                this, FirmEditController.class);
        firm = Firm_;
        nameFirmTarget.setPromptText(firm.getNameFirm());
        employees = JDBCSQL.JDBCFirms.getEmployees(firm.getIDFirm());
        employees.add(0, new Employee("nune"));
        listPersTarget.setItems(employees);
        Employee ee = getChief();
        if (ee != null) {
            listPersTarget.setValue(ee);
        } else {
            listPersTarget.setValue(employees.get(0));
        }
    }

    private Employee getChief() {
        for (Employee emp : employees) {
            if (emp.isChif() == true) {
                return emp;
            }
        }
        return null;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        createrTarget.setText("Создатель: " + UserTemp.getUserName());

    }

    public void applyFirmNameButtonAction(ActionEvent actionEvent) {
        if (JDBCSQL.JDBCFind.Firm(nameFirmTarget.getText())) {
            actiontarget.setText("Такая фирма уже существует!");
        } else {
            // JDBCSQL.JDBCFirms.addNewFirm(nameFirmTarget.getText());
            JDBCSQL.JDBCFirms.updateName(firm.getIDFirm(), nameFirmTarget.getText());
            firm.setNameFirm(nameFirmTarget.getText());
            nameFirmTarget.setText("");
            nameFirmTarget.setPromptText(firm.getNameFirm());
            MessageBox.show(this, "Применение закончено!", "Применение закончено!", MessageBox.ICON_INFORMATION | MessageBox.OK | MessageBox.CANCEL);
        }
    }

    public void applyDirectorButtonAction(ActionEvent actionEvent) {
        Employee value = (Employee) listPersTarget.getValue();
        for (Employee emp : employees) {
            if (emp != employees.get(0)) {
                if (value.getAppUserId().compareTo(emp.getAppUserId()) == 0) {
                    JDBCSQL.JDBCFirms.updateChief(firm.getIDFirm(), emp.getAppUserId(), true);
                    emp.setChif(true);

                } else {
                    JDBCSQL.JDBCFirms.updateChief(firm.getIDFirm(), emp.getAppUserId(), false);
                    emp.setChif(false);
                }
            }
        }
        MessageBox.show(this, "Применение закончено!", "Применение закончено!", MessageBox.ICON_INFORMATION | MessageBox.OK | MessageBox.CANCEL);
        // close();
    }
}