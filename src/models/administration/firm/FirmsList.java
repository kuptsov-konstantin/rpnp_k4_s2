package models.administration.firm;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class FirmsList {
    private ObservableList<Firm> namesOrg = FXCollections.observableArrayList();
    private String ApplicationUserID;

    public FirmsList() {

    }

    public FirmsList(String applicationUserID) {
        ApplicationUserID = applicationUserID;
    }

    public FirmsList(ObservableList<Firm> fl) {
        namesOrg = fl;
    }

    public String getApplicationUserID() {
        return ApplicationUserID;
    }

    public void setApplicationUserID(String applicationUserID) {
        ApplicationUserID = applicationUserID;
    }

    public void addNewFirm(String str, int idFirm) {
        if (namesOrg == null) {
            namesOrg = FXCollections.observableArrayList();
        }
        namesOrg.add(new Firm(str, idFirm));
    }

    public ObservableList<Firm> getNamesOrg() {
        return namesOrg;
    }

    public void setNamesOrg(ObservableList<Firm> namesOrg) {
        this.namesOrg = namesOrg;
    }
}
