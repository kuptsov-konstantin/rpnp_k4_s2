package models.administration.firm.department;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import models.JDBCSQL;
import models.MainStage;
import models.administration.firm.Firm;
import models.employees.People;
import models.param.UserTemp;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by Константин on 06.03.2016.
 */
public class DepartmentAddController extends MainStage implements Initializable {
    public TextField middleTarget;
    public TextField nameTarget;
    public TextField familyTarget;
    public ComboBox listFirmTarget;
    public TextField emailTarget;
    public PasswordField rpassTarget;
    public PasswordField passTarget;
    public TextField loginTarget;
    public Text createrTarget;
    public ListView listViewTarget;
    public Text errorTarget;
    public DatePicker birthDateTarget;

    public DepartmentAddController() {
        loadFxml(getClass().getResource("/gui/administration/user/UserAdd.fxml"), "Добавление пользователя", 450, 500, this, DepartmentAddController.class);
    }

    public void addToFirmButtonAction(ActionEvent actionEvent) {
        Firm firm = (Firm) listFirmTarget.getValue();
        boolean find_firm = false;
        for (Firm _firm : (ObservableList<Firm>) listViewTarget.getItems()) {
            if (_firm.getIDFirm() == firm.getIDFirm()) {
                find_firm = true;
            }
        }
        if (!find_firm) {
            listViewTarget.getItems().add(firm);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listFirmTarget.setItems(JDBCSQL.getPersonFirms(UserTemp.getApplicationUserID()));
        listFirmTarget.getSelectionModel().select(0);
        listViewTarget.setCellFactory(param -> new ButtonListCell());
        rpassTarget.setOnKeyReleased(event -> {
                    if (rpassTarget.getText().compareTo(passTarget.getText()) != 0) {
                        errorTarget.setText("А пароли то.. НЕ СОВПАДАЮТ!");
                    } else {
                        errorTarget.setText("Все ок!");
                    }
                }
        );

        birthDateTarget.setValue(LocalDate.now());
    }

    public void cancelAction(ActionEvent actionEvent) {

        super.hide();
    }

    public void registerAction(ActionEvent actionEvent) {
        if (birthDateTarget.getValue().compareTo(LocalDate.now().minusYears(18)) == -1) {
            errorTarget.setText("Пади с датой напутали!");
        }
        if (familyTarget.getText().length() == 0) {
            errorTarget.setText("Нужно ввести фамилию!");
            return;
        }
        if (nameTarget.getText().length() == 0) {
            errorTarget.setText("Нужно ввести имя!");
            return;
        }
        if (middleTarget.getText().length() == 0) {
            errorTarget.setText("Нужно ввести отчество!");
            return;
        }
        if (passTarget.getText().length() == 0) {
            errorTarget.setText("Поле пароля пустое!");
            return;
        }
        if (rpassTarget.getText().length() == 0) {
            errorTarget.setText("Поле повторить пароль пустое!");
            return;
        }
        if (loginTarget.getText().length() == 0) {
            errorTarget.setText("Поле логин пустое!");
            return;
        }
        if (emailTarget.getText().length() == 0) {
            errorTarget.setText("Поле емэйл пустое!");
            return;
        }
        if (listViewTarget.getItems().size() == 0) {
            errorTarget.setText("Добавьте хоть одну фирму!");
            return;
        }

        errorTarget.setText("");
        boolean blogin = JDBCSQL.JDBCFind.Login(loginTarget.getText());
        if (blogin) {
            errorTarget.setText("Такой логин уже есть!");
        }
        boolean bemail = JDBCSQL.JDBCFind.EMail(emailTarget.getText());
        if (bemail) {
            if (blogin) {
                errorTarget.setText(errorTarget.getText() + " " + "Такое мыло уже есть!");
            } else {
                errorTarget.setText("Такое мыло уже есть!");
            }
        } else {
            if (blogin == false) {
                if (JDBCSQL.JDBCUser.register(loginTarget.getText(), passTarget.getText(), emailTarget.getText(), new People(familyTarget.getText(), nameTarget.getText(), middleTarget.getText(), birthDateTarget.getValue()), listViewTarget.getItems()) == false) {
                    errorTarget.setText("Не зарегило!");
                } else {
                    this.hide();
                }
            }
        }
    }

    class ButtonListCell extends ListCell<Firm> {
        @Override
        public void updateItem(Firm obj, boolean empty) {
            super.updateItem(obj, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setText(obj.toString());
                Button butt = new Button("X");
                butt.setOnAction(event -> {
                    int findFirmID = -1, count = 0;
                    for (Firm _firm : (ObservableList<Firm>) listViewTarget.getItems()) {
                        if (_firm.getIDFirm() == obj.getIDFirm()) {
                            findFirmID = count;
                        }
                        count++;
                    }
                    listViewTarget.getItems().remove(findFirmID);
                });
                setGraphic(butt);
            }
        }
    }
}
