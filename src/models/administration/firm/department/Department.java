package models.administration.firm.department;

/**
 * Created by Константин on 16.03.2016.
 */
public class Department {
    private String nameDep;
    private int idDep;
    private int idFirm;
    private int colVoSotr;

    public Department() {

    }

    public Department(String _nameDep, int _idDep, int _idFirm, int _colVoSotr) {
        idDep = _idDep;
        idFirm = _idFirm;
        nameDep = _nameDep;
        colVoSotr = _colVoSotr;
    }

    public int getIdDep() {
        return idDep;
    }

    public void setIdDep(int idDep) {
        this.idDep = idDep;
    }

    public int getIdFirm() {
        return idFirm;
    }

    public void setIdFirm(int idFirm) {
        this.idFirm = idFirm;
    }

    public String getNameDep() {
        return nameDep;
    }

    public void setNameDep(String nameDep) {
        this.nameDep = nameDep;
    }

    public int getColVoSotr() {
        return colVoSotr;
    }

    public void setColVoSotr(int colVoSotr) {
        this.colVoSotr = colVoSotr;
    }

    @Override
    public String toString() {
        return nameDep + " с (" + colVoSotr + ") сотрудниками";
    }
}
