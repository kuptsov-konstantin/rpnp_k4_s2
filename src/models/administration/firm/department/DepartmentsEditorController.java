package models.administration.firm.department;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import models.JDBCSQL;
import models.MainStage;
import models.administration.firm.Firm;
import models.param.UserTemp;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Константин on 16.03.2016.
 */
public class DepartmentsEditorController extends MainStage implements Initializable {
    public Text employeeFirmTarget;
    public TextField addDepsTarget;
    public ListView firmstarget;
    public ListView depsTarget;
    private ObservableList<Firm> olf = FXCollections.observableArrayList();
    private ObservableList<Department> old = FXCollections.observableArrayList();

    public DepartmentsEditorController() {
        loadFxml(getClass().getResource("/gui/administration/firm/department/DepartmentsEditor.fxml"), "Отделы", 650, 500, this, DepartmentsEditorController.class);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        olf = JDBCSQL.getPersonFirms(UserTemp.getApplicationUserID());
        //   olf.add(0, new Firm("Все", -1));
        firmstarget.setItems(olf);
        firmstarget.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Firm>() {
                    public void changed(ObservableValue<? extends Firm> ov, Firm old_val, Firm new_val) {
                        if (new_val != null) {
                            employeeFirmTarget.setText("Отделы фирмы " + new_val.getNameFirm());
                            if (new_val.getIDFirm() != -1) {
                                old.clear();
                                old = JDBCSQL.JDBCDepartments.getDepartments(new_val.getIDFirm());
                            } else {
                                ObservableList<Integer> mas = FXCollections.observableArrayList();
                                for (Firm firm : (ObservableList<Firm>) firmstarget.getItems()) {
                                    if (firm.getIDFirm() > -1) {
                                        mas.add(firm.getIDFirm());
                                    }
                                }
                                old.clear();
                                old = JDBCSQL.JDBCDepartments.getDepartments(mas);

                            }
                            depsTarget.setItems(old);
                        }
                    }
                });
        firmstarget.getSelectionModel().select(0);
        depsTarget.setItems(old);
        ContextMenu menu = new ContextMenu();
        MenuItem izm_item = new MenuItem("Изменить");
        MenuItem del_item = new MenuItem("Удалить");
        // izm_item.setOnAction(event -> System.out.println("you've clicked the menu item"));
        izm_item.setOnAction(event -> {
            Department department = (Department) depsTarget.getSelectionModel().getSelectedItem();
            System.out.println(department.toString());
            DepartmentEditController uec = new DepartmentEditController(department);
            uec.show();
        });
        del_item.setOnAction(event -> {
            Department dep = (Department) depsTarget.getSelectionModel().getSelectedItem();
            if (JDBCSQL.JDBCDepartments.delDepartment(dep.getIdDep())) {
                old = JDBCSQL.JDBCDepartments.getDepartments(dep.getIdFirm());
                depsTarget.setItems(old);
            }

        });
        menu.getItems().addAll(izm_item, del_item);
        depsTarget.setContextMenu(menu);
    }

    public void addDepToFirm(ActionEvent actionEvent) {
        if (addDepsTarget.getText().length() > 0) {
            Firm firm = (Firm) firmstarget.getSelectionModel().getSelectedItem();
            boolean b = JDBCSQL.JDBCDepartments.addDepartmetnToFirm(addDepsTarget.getText(), firm.getIDFirm());
            if (b == true) {
                old = JDBCSQL.JDBCDepartments.getDepartments(firm.getIDFirm());
                addDepsTarget.setText("");
                depsTarget.setItems(old);
            }
        }

    }
}
