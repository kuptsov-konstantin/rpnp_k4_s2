package models.administration.firm.department;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import models.JDBCSQL;
import models.MainStage;
import models.employees.Employee;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Константин on 06.03.2016.
 */
public class DepartmentEditController extends MainStage implements Initializable {

    public Text createrTarget;
    public ListView listEmployeesTarget;
    public TextField nameDepTarget;
    public ComboBox ComboBoxEmployeeTarget;
    private String AppUserID;
    private Department Dep;

    //ObservableList<Employee> departmentEmployee = FXCollections.observableArrayList();
    public DepartmentEditController() {
        loadFxml(getClass().getResource("/gui/administration/firm/department/DepartmentEdit.fxml"), "Редактирвоание отдела", 400, 500, this, DepartmentEditController.class);
    }

    public DepartmentEditController(String appUserID) {
        this.AppUserID = appUserID;
        loadFxml(getClass().getResource("/gui/administration/firm/department/DepartmentEdit.fxml"), "Редактирвоание отдела", 400, 500, this, DepartmentEditController.class);
    }

    public DepartmentEditController(Department _Dep) {
        this.Dep = _Dep;
        loadFxml(getClass().getResource("/gui/administration/firm/department/DepartmentEdit.fxml"), "Редактирвоание отдела", 400, 500, this, DepartmentEditController.class);

    }

    public void addToEmployeesDepButtonAction(ActionEvent actionEvent) {

        listEmployeesTarget.getItems().add(ComboBoxEmployeeTarget.getSelectionModel().getSelectedItem());
    }

    public void cancelButtonHandle(ActionEvent actionEvent) {
        this.hide();
    }

    public void applyButtonHandle(ActionEvent actionEvent) {
        JDBCSQL.JDBCDepartments.setDepartment(Dep.getIdDep(), nameDepTarget.getText(), listEmployeesTarget.getItems());
        this.hide();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //   departmentEmployee  = ;
        listEmployeesTarget.setItems(JDBCSQL.JDBCDepartments.getDepartmentEmployee(Dep.getIdDep()));
        listEmployeesTarget.setCellFactory(param -> new ButtonListCell());
        ComboBoxEmployeeTarget.setItems(JDBCSQL.JDBCFirms.getEmployees(Dep.getIdFirm()));
        listEmployeesTarget.getSelectionModel().select(0);
        nameDepTarget.setText(Dep.getNameDep());

    }

    class ButtonListCell extends ListCell<Employee> {
        @Override
        public void updateItem(Employee obj, boolean empty) {
            super.updateItem(obj, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setText(obj.toString());
                Button butt = new Button("-");
                butt.setOnAction(event -> {
                    System.out.println(obj.toString() + " " + obj.getAppUserId());
                    listEmployeesTarget.getItems().remove(obj);

                    // JDBCSQL.JDBCFirms.delFirm(obj.getIDFirm());
                    // listEmployeesTarget.setItems( JDBCSQL.JDBCDepartments.getDepartmentEmployee(Dep.getIdDep()));

                });
                setGraphic(butt);
               /* if (withDelTarget.isSelected() == false) {
                    HBox hbox = new HBox();
                    Button butt = new Button("Удалить");
                    Button butt1 = new Button("Изменить");
                    butt.setOnAction(event -> {
                        System.out.println(obj.getNameFirm() + " " + obj.getIDFirm());
                        JDBCSQL.JDBCFirms.delFirm(obj.getIDFirm());
                        update(false);
                    });

                    // setGraphic(butt);
                    butt1.setOnAction(event -> {
                        edit(obj);
                        // System.out.println("clicked");
                        update(false);
                    });
                    hbox.getChildren().addAll(butt, butt1);
                    setGraphic(hbox);
                }else {
                    Button butt = new Button("Восстановить");
                    butt.setOnAction(event -> {
                        System.out.println(obj.getNameFirm() + " " + obj.getIDFirm());
                        JDBCSQL.JDBCFirms.returnFirm(obj.getIDFirm());
                        update(true);
                    });
                    setGraphic(butt);
                }*/
            }
        }
    }
}
