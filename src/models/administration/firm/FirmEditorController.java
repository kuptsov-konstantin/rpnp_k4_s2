package models.administration.firm;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import models.JDBCSQL;
import models.MainStage;
import models.param.UserTemp;

import java.net.URL;
import java.util.ResourceBundle;


public class FirmEditorController extends MainStage implements Initializable {
    public ListView firmsTarget;
    public TextField newFirmTarget;
    public Text actiontarget;
    public CheckBox withDelTarget;

    public FirmEditorController() {
        // super("/gui/administration/firm/FirmEditor.fxml", "Редактирвоание фирм", 400, 500);
        super.loadFxml(
                getClass().getResource("/gui/administration/firm/FirmEditor.fxml"),
                "Редактирвоание фирм", 400, 570,
                this, FirmEditorController.class);

    }

    @FXML
    public void handleAddFirmButtonAction(ActionEvent actionEvent) {
        if (newFirmTarget.getText().length() > 0) {
            if (JDBCSQL.JDBCFind.Firm(newFirmTarget.getText())) {
                actiontarget.setText("Такая фирма уже существует!");
            } else {
                if (JDBCSQL.JDBCFirms.addNewFirm(newFirmTarget.getText())) {
                    actiontarget.setText("Фирма добавлена в список!");
                    update(false);
                } else {
                    actiontarget.setText("Произошла ошибка!");
                }
            }
        }
        /*FirmAddController fac = new FirmAddController();
        fac.initModality(Modality.APPLICATION_MODAL);
        fac.initOwner(this);
        fac.show();*/
    }

    @FXML
    public void handleDeleteFirmButtonAction(ActionEvent actionEvent) {
    }

    @FXML
    public void handleEditFirmButtonAction(ActionEvent actionEvent) {

    }

    void edit(Firm firm) {
        FirmEditController fec = new FirmEditController(firm);
        fec.initModality(Modality.APPLICATION_MODAL);
        fec.initOwner(this);
        fec.show();
    }

    void update(boolean withDel) {
        firmsTarget.setItems(JDBCSQL.JDBCFirms.getFirms(UserTemp.getApplicationUserID(), withDel).getNamesOrg());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firmsTarget.setCellFactory(param -> new ButtonListCell());
        update(false);
    }

    public void withDelAction(ActionEvent actionEvent) {
        update(withDelTarget.isSelected());
    }

    class ButtonListCell extends ListCell<Firm> {
        @Override
        public void updateItem(Firm obj, boolean empty) {
            super.updateItem(obj, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setText(obj.toString());

                if (withDelTarget.isSelected() == false) {
                    HBox hbox = new HBox();
                    Button butt = new Button("Удалить");
                    Button butt1 = new Button("Изменить");
                    butt.setOnAction(event -> {
                        System.out.println(obj.getNameFirm() + " " + obj.getIDFirm());
                        JDBCSQL.JDBCFirms.delFirm(obj.getIDFirm());
                        update(false);
                    });

                    // setGraphic(butt);
                    butt1.setOnAction(event -> {
                        edit(obj);
                        // System.out.println("clicked");
                        update(false);
                    });
                    hbox.getChildren().addAll(butt, butt1);
                    setGraphic(hbox);
                } else {
                    Button butt = new Button("Восстановить");
                    butt.setOnAction(event -> {
                        System.out.println(obj.getNameFirm() + " " + obj.getIDFirm());
                        JDBCSQL.JDBCFirms.returnFirm(obj.getIDFirm());
                        update(true);
                    });
                    setGraphic(butt);
                }
            }
        }
    }
}
