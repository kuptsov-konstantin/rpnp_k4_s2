package models.administration.firm.project;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import models.JDBCSQL;
import models.MainStage;
import models.administration.firm.Firm;
import models.administration.firm.FirmsList;
import models.employees.EmployeeTable;
import models.param.UserTemp;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by Константин on 06.03.2016.
 */
public class ProjectAddController extends MainStage implements Initializable {
    public ComboBox comboboxFirm;
    public ComboBox combomoxEmployee;
    public ListView listEmployeeInProject;
    public DatePicker deadLine;
    public TextField projectName;
    public DatePicker startDate;

    public ProjectAddController() {
        loadFxml(getClass().getResource("/gui/administration/firm/project/ProjectAdd.fxml"), "Добавление проекта", 450, 500, this, ProjectAddController.class);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Firm> personFirms = JDBCSQL.getPersonFirms(UserTemp.getApplicationUserID());
        comboboxFirm.setItems(personFirms);
        comboboxFirm.getSelectionModel().select(0);
        combomoxEmployee.setItems(JDBCSQL.getEmployees(new FirmsList(personFirms)));
        combomoxEmployee.getSelectionModel().select(0);
        //listFirmInProject.setCellFactory(param -> new ButtonListCellListFirmInProject());
        listEmployeeInProject.setCellFactory(param -> new ButtonListCellListEmployeeInProject());
        deadLine.setValue(LocalDate.now());
        startDate.setValue(LocalDate.now());
    }

    public void addEmpToProject(ActionEvent actionEvent) {
        EmployeeTable firm = (EmployeeTable) combomoxEmployee.getValue();
        boolean find_firm = false;
        for (EmployeeTable _firm : (ObservableList<EmployeeTable>) listEmployeeInProject.getItems()) {
            if (_firm.getAppUserID().compareTo(firm.getAppUserID()) == 0) {
                find_firm = true;
            }
        }
        if (!find_firm) {
            listEmployeeInProject.getItems().add(firm);
        }
    }

    public void handleCancel(ActionEvent actionEvent) {
        super.hide();
    }

    public void handleAdd(ActionEvent actionEvent) {
        if (deadLine.getValue().compareTo(startDate.getValue()) < 0) {
            //   errorTarget.setText("Пади с датой напутали!");
            return;
        }
        if (projectName.getText().length() == 0) {
            // errorTarget.setText("Нужно ввести фамилию!");
            return;
        }
       /* if (listFirmInProject.getItems().size() == 0) {
            // errorTarget.setText("Нужно ввести имя!");
            return;
        }*/
        if (listEmployeeInProject.getItems().size() == 0) {
            // errorTarget.setText("Нужно ввести отчество!");
            return;
        }

        boolean dd = JDBCSQL.JDBCProject.addProject(projectName.getText(), startDate.getValue(), deadLine.getValue(), (Firm) comboboxFirm.getSelectionModel().getSelectedItem(), listEmployeeInProject.getItems());
        if (dd == true) {
            this.hide();
        }
    }

    /*
        class ButtonListCellListFirmInProject extends ListCell<Firm> {
            @Override
            public void updateItem(Firm obj, boolean empty) {
                super.updateItem(obj, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(obj.toString());
                    Button butt = new Button("X");
                    butt.setOnAction(event -> {
                        int findFirmID = -1, count = 0;
                        for (Firm _firm: (ObservableList<Firm>) listFirmInProject.getItems()) {
                            if(_firm.getIDFirm() == obj.getIDFirm()){
                                findFirmID = count;
                            }
                            count++;
                        }
                        listFirmInProject.getItems().remove(findFirmID);
                    });
                    setGraphic(butt);
                }
            }
        }*/
    class ButtonListCellListEmployeeInProject extends ListCell<EmployeeTable> {
        @Override
        public void updateItem(EmployeeTable obj, boolean empty) {
            super.updateItem(obj, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setText(obj.toString());
                Button butt = new Button("X");
                butt.setOnAction(event -> {
                    int findFirmID = -1, count = 0;
                    for (EmployeeTable _firm : (ObservableList<EmployeeTable>) listEmployeeInProject.getItems()) {
                        if (_firm.getAppUserID().compareTo(obj.getAppUserID()) == 0) {
                            findFirmID = count;
                        }
                        count++;
                    }
                    listEmployeeInProject.getItems().remove(findFirmID);
                });
                setGraphic(butt);
            }
        }
    }
}
