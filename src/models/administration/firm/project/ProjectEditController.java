package models.administration.firm.project;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import models.JDBCSQL;
import models.MainStage;
import models.administration.firm.Firm;
import models.administration.firm.FirmsList;
import models.administration.firm.projects.Project;
import models.employees.Employee;
import models.param.UserTemp;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Константин on 06.03.2016.
 */
public class ProjectEditController extends MainStage implements Initializable {

    public TextField projectName;
    public DatePicker startDate;
    public DatePicker deadLine;
    public ComboBox comboboxFirm;
    public ComboBox combomoxEmployee;
    public ListView listEmployeeInProject;
    private Project _project = new Project();

    public ProjectEditController() {
        loadFxml(getClass().getResource("/gui/administration/firm/project/ProjectEditor.fxml"), "Редактирвоание пользователя", 400, 500, this, ProjectEditController.class);
    }

    public ProjectEditController(Project prj) {
        this._project = prj;
        loadFxml(getClass().getResource("/gui/administration/firm/project/ProjectEditor.fxml"), "Редактирвоание пользователя", 400, 500, this, ProjectEditController.class);
    }

    int findInFirms(ObservableList<Firm> firmObservableList, int idFirm) {
        int count = 0;
        for (Firm firm : firmObservableList) {
            if (firm.getIDFirm() == idFirm) {
                return count;
            }
            count++;
        }
        return -1;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Project project = JDBCSQL.JDBCProject.getProject(_project.getProjectID());
        projectName.setText(project.getProjectName());
        ObservableList<Employee> projectEmployee = JDBCSQL.JDBCProject.getProjectEmployee(project.getProjectID());
        ObservableList<Firm> personFirms = JDBCSQL.getPersonFirms(UserTemp.getApplicationUserID());
        comboboxFirm.setItems(personFirms);
        comboboxFirm.getSelectionModel().select(findInFirms(personFirms, project.getFirmID()));
        combomoxEmployee.setItems(JDBCSQL.JDBCFirms.getEmployees(new FirmsList(personFirms)));
        combomoxEmployee.getSelectionModel().select(0);
        //listFirmInProject.setCellFactory(param -> new ButtonListCellListFirmInProject());
        listEmployeeInProject.setItems(projectEmployee);
        listEmployeeInProject.setCellFactory(param -> new ButtonListCellListEmployeeInProject());
        deadLine.setValue(project.getEndDate().toLocalDate());
        startDate.setValue(project.getStartDate().toLocalDate());
    }

    public void addEmpToProject(ActionEvent actionEvent) {
        Employee employee = (Employee) combomoxEmployee.getValue();
        boolean find_firm = false;
        for (Employee _firm : (ObservableList<Employee>) listEmployeeInProject.getItems()) {
            if (_firm.getAppUserId().compareTo(employee.getAppUserId()) == 0) {
                find_firm = true;
            }
        }
        if (!find_firm) {
            listEmployeeInProject.getItems().add(employee);
        }
    }

    public void handleCancel(ActionEvent actionEvent) {
    }

    public void handleApply(ActionEvent actionEvent) {
        if (deadLine.getValue().compareTo(startDate.getValue()) < 0) {
            //   errorTarget.setText("Пади с датой напутали!");
            return;
        }
        if (projectName.getText().length() == 0) {
            // errorTarget.setText("Нужно ввести фамилию!");
            return;
        }
       /* if (listFirmInProject.getItems().size() == 0) {
            // errorTarget.setText("Нужно ввести имя!");
            return;
        }*/
        if (listEmployeeInProject.getItems().size() == 0) {
            // errorTarget.setText("Нужно ввести отчество!");
            return;
        }

        ObservableList<Employee> projectEmployee = JDBCSQL.JDBCProject.getProjectEmployee(_project.getProjectID());
        ObservableList<String> for_delete_list_userId = FXCollections.observableArrayList();
        ObservableList<String> for_add_list_userId = FXCollections.observableArrayList();

        assert projectEmployee != null;
        for (Employee employee : projectEmployee) {
            boolean isFinde = false;
            for (Employee employeeInList : (ObservableList<Employee>) listEmployeeInProject.getItems()) {
                if (employee.getAppUserId().compareTo(employeeInList.getAppUserId()) == 0) {
                    isFinde = true;
                }
            }
            if (!isFinde) {
                for_delete_list_userId.add(employee.getAppUserId());
            }
        }
        for (Employee employeeInList : (ObservableList<Employee>) listEmployeeInProject.getItems()) {
            boolean isFinde = false;
            for (Employee employee : projectEmployee) {
                if (employee.getAppUserId().compareTo(employeeInList.getAppUserId()) == 0) {
                    isFinde = true;
                }
            }
            if (!isFinde) {
                for_add_list_userId.add(employeeInList.getAppUserId());
            }
        }
        for (String employee : for_delete_list_userId) {
            JDBCSQL.JDBCProject.delProjectEmployeeFirms(_project.getProjectID(), employee);
        }
        for (String employee : for_add_list_userId) {
            JDBCSQL.JDBCProject.addProjectEmployeeFirms(_project.getProjectID(), employee);
        }
        boolean dd = JDBCSQL.JDBCProject.updateProject(_project.getProjectID(), projectName.getText(), startDate.getValue(), deadLine.getValue(), (Firm) comboboxFirm.getSelectionModel().getSelectedItem());
        if (dd) {
            this.hide();
        }
    }

    class ButtonListCellListEmployeeInProject extends ListCell<Employee> {
        @Override
        public void updateItem(Employee obj, boolean empty) {
            super.updateItem(obj, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setText(obj.toString());
                Button butt = new Button("X");
                butt.setOnAction(event -> {
                    int findFirmID = -1, count = 0;
                    for (Employee _firm : (ObservableList<Employee>) listEmployeeInProject.getItems()) {
                        if (_firm.getAppUserId().compareTo(obj.getAppUserId()) == 0) {
                            findFirmID = count;
                        }
                        count++;
                    }
                    listEmployeeInProject.getItems().remove(findFirmID);
                });
                setGraphic(butt);
            }
        }
    }
}
