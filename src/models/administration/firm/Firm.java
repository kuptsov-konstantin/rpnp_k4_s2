package models.administration.firm;

public class Firm {
    private String NameFirm = "";
    private int IDFirm = -1;

    public Firm() {
    }

    public Firm(String nameFirm, int idFirm) {
        this.IDFirm = idFirm;
        this.NameFirm = nameFirm;
    }

    public int getIDFirm() {
        return IDFirm;
    }

    public void setIDFirm(int IDFirm) {
        this.IDFirm = IDFirm;
    }

    public String getNameFirm() {
        return NameFirm;
    }

    public void setNameFirm(String nameFirm) {
        NameFirm = nameFirm;
    }

    @Override
    public String toString() {
        return this.NameFirm;
    }
}
