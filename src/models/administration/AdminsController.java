package models.administration;

import javafx.event.ActionEvent;
import models.MainStage;
import models.administration.firm.FirmEditorController;
import models.administration.firm.department.DepartmentsEditorController;
import models.employees.EmployeesController;

public class AdminsController extends MainStage {
    private String ApplicationUserID;

    public AdminsController() {
        // super("/gui/Admins.fxml", "Админка", 400.0, 500.0);
        super.loadFxml(getClass().getResource("/gui/Admins.fxml"), "Админка", 400.0, 500.0, this, AdminsController.class);

    }

    public void handleFirmsEditorButtonAction(ActionEvent actionEvent) {
        FirmEditorController fec = new FirmEditorController();
        fec.setPreviousWindow(this);
        fec.show();
        this.nextStage();
    }

    public void setApplicationUserID(String applicationUserID) {
        ApplicationUserID = applicationUserID;
    }

    public void handleDepartmentsEditorButtonAction(ActionEvent actionEvent) {
        DepartmentsEditorController fec = new DepartmentsEditorController();
        fec.setPreviousWindow(this);
        fec.show();
        this.nextStage();
    }

    public void handleUsersEditorButtonAction(ActionEvent actionEvent) {
        EmployeesController fec = new EmployeesController();
        fec.setPreviousWindow(this);
        fec.show();
        this.nextStage();
    }

  /*  public void handleAddFirmButtonAction(ActionEvent actionEvent) {
    }

    public void handleDeleteFirmButtonAction(ActionEvent actionEvent) {
    }

    public void handleEditFirmButtonAction(ActionEvent actionEvent) {

    }*/
}
