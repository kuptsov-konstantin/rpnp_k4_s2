package models.administration.user;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import models.JDBCSQL;
import models.MainStage;
import models.administration.firm.Firm;
import models.param.UserTemp;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by Константин on 06.03.2016.
 */
public class UserEditController extends MainStage implements Initializable {

    public Text createrTarget;
    public TextField loginTarget;
    public PasswordField passTarget;
    public PasswordField rpassTarget;
    public TextField emailTarget;
    public TextField familyTarget;
    public TextField nameTarget;
    public TextField middleTarget;
    public ComboBox listFirmTarget;
    public DatePicker birthDateTarget;
    public ListView listViewTarget;
    public Text errorTarget;
    User user;
    private String AppUserID;

    public UserEditController() {
        loadFxml(getClass().getResource("/gui/administration/user/UserEdit.fxml"), "Редактирвоание пользователя", 550, 500, this, UserEditController.class);
    }

    public UserEditController(String appUserID) {
        this.AppUserID = appUserID;
        loadFxml(getClass().getResource("/gui/administration/user/UserEdit.fxml"), "Редактирвоание пользователя", 550, 500, this, UserEditController.class);
    }

    public void addToFirmButtonAction(ActionEvent actionEvent) {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        user = JDBCSQL.JDBCUser.getPerson(AppUserID);
        listFirmTarget.setItems(JDBCSQL.getPersonFirms(UserTemp.getApplicationUserID()));
        listFirmTarget.getSelectionModel().select(0);
        listViewTarget.setCellFactory(param -> new ButtonListCell());
        rpassTarget.setOnKeyReleased(event -> {
                    if (rpassTarget.getText().compareTo(passTarget.getText()) != 0) {
                        errorTarget.setText("А пароли то.. НЕ СОВПАДАЮТ!");
                    } else {
                        errorTarget.setText("Все ок!");
                    }
                }
        );
        listViewTarget.setItems(user.getIdFirms().getNamesOrg());
        birthDateTarget.setValue(user.getBirthDateTargetValue());
        loginTarget.setText(user.getLogin());
        emailTarget.setText(user.getEmailP());
        nameTarget.setText(user.getName());
        middleTarget.setText(user.getMiddle());
        familyTarget.setText(user.getFamily());

    }

    public void cancelAction(ActionEvent actionEvent) {
        super.hide();
    }

    public void applyAction(ActionEvent actionEvent) {
        if (birthDateTarget.getValue().compareTo(LocalDate.now().minusYears(18)) == -1) {
            errorTarget.setText("Пади с датой напутали!");
            return;
        } else {
            if (birthDateTarget.getValue().compareTo(user.getBirthDateTargetValue()) != 0) {
                JDBCSQL.JDBCUser.setBirthDay(AppUserID, birthDateTarget.getValue());
            }
        }

        if (familyTarget.getText().length() == 0) {
            errorTarget.setText("Нужно ввести фамилию!");
            return;
        } else {
            if (familyTarget.getText().compareTo(user.getFamily()) != 0) {
                JDBCSQL.JDBCUser.setFamily(AppUserID, familyTarget.getText());
            }
        }
        if (nameTarget.getText().length() == 0) {
            errorTarget.setText("Нужно ввести имя!");
            return;
        } else {
            if (nameTarget.getText().compareTo(user.getName()) != 0) {
                JDBCSQL.JDBCUser.setName(AppUserID, nameTarget.getText());
            }
        }
        if (middleTarget.getText().length() == 0) {
            errorTarget.setText("Нужно ввести отчество!");
            return;
        } else {
            if (middleTarget.getText().compareTo(user.getMiddle()) != 0) {
                JDBCSQL.JDBCUser.setMiddleName(AppUserID, middleTarget.getText());
            }
        }

        if (loginTarget.getText().length() == 0) {
            errorTarget.setText("Поле логин пустое!");
            return;
        } else {
            if (loginTarget.getText().compareTo(user.getLogin()) != 0) {
                boolean blogin = JDBCSQL.JDBCFind.Login(loginTarget.getText());
                if (blogin) {
                    errorTarget.setText("Такой логин уже есть!");
                    return;
                }
                JDBCSQL.JDBCUser.setLogin(AppUserID, loginTarget.getText());
            }
        }
        if (emailTarget.getText().length() == 0) {
            errorTarget.setText("Поле емэйл пустое!");
            return;
        } else {
            if (emailTarget.getText().compareTo(user.getEmailP()) != 0) {
                JDBCSQL.JDBCUser.setEMail(AppUserID, emailTarget.getText());
            }
        }


        if (passTarget.getText().length() == 0) {
            errorTarget.setText("Поле пароля пустое!");
            //return;
        } else {
            if (rpassTarget.getText().length() == 0) {
                errorTarget.setText("Поле повторить пароль пустое!");
                // return;
            } else {
                if (rpassTarget.getText().compareTo(passTarget.getText()) == 0) {
                    JDBCSQL.JDBCUser.setPass(AppUserID, loginTarget.getText(), emailTarget.getText(), passTarget.getText());
                    // errorTarget.setText("А пароли то.. НЕ СОВПАДАЮТ!");
                } else {
                    errorTarget.setText("Все ок!");
                }
            }
        }

        if (listViewTarget.getItems().size() == 0) {
            errorTarget.setText("Добавьте хоть одну фирму!");
            return;
        } else {
            JDBCSQL.JDBCUser.setFirms(AppUserID, listViewTarget.getItems());

        }

        errorTarget.setText("");

      /*  boolean bemail = JDBCSQL.JDBCFind.EMail(emailTarget.getText());
        if (bemail) {
            if (blogin) {
                errorTarget.setText(errorTarget.getText() + " " + "Такое мыло уже есть!");
            } else {
                errorTarget.setText("Такое мыло уже есть!");
            }
        } else {
            if (blogin == false) {
                if (JDBCSQL.JDBCUser.register(loginTarget.getText(), passTarget.getText(), emailTarget.getText(), new People(familyTarget.getText(), nameTarget.getText(), middleTarget.getText(), birthDateTarget.getValue()), listViewTarget.getItems()) == false) {
                    errorTarget.setText("Не зарегило!");
                } else {
                    this.hide();
                }
            }
        }*/
    }

    class ButtonListCell extends ListCell<Firm> {
        @Override
        public void updateItem(Firm obj, boolean empty) {
            super.updateItem(obj, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setText(obj.toString());
                Button butt = new Button("X");
                butt.setOnAction(event -> {
                    int findFirmID = -1, count = 0;
                    for (Firm _firm : (ObservableList<Firm>) listViewTarget.getItems()) {
                        if (_firm.getIDFirm() == obj.getIDFirm()) {
                            findFirmID = count;
                        }
                        count++;
                    }
                    listViewTarget.getItems().remove(findFirmID);
                });
                setGraphic(butt);
            }
        }
    }
}
