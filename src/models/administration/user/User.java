package models.administration.user;

import models.administration.firm.FirmsList;
import models.employees.People;

public class User extends People {
    private String emailP;
    private String newPass;
    private String newPassRep;
    private String login;
    private FirmsList idFirms;

    public User() {

    }

    public User(String family, String name, String middle) {
        super(family, name, middle);
    }

    public String getEmailP() {
        return emailP;
    }

    public void setEmailP(String emailP) {
        this.emailP = emailP;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getNewPassRep() {
        return newPassRep;
    }

    public void setNewPassRep(String newPassRep) {
        this.newPassRep = newPassRep;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public FirmsList getIdFirms() {
        return idFirms;
    }

    public void setIdFirms(FirmsList idFirms) {
        this.idFirms = idFirms;
    }
}
