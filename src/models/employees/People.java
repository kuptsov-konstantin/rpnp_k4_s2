package models.employees;

import java.sql.Date;
import java.time.LocalDate;

/**
 * Created by Константин on 05.03.2016.
 */
public class People {
    private String Family;
    private String Name;
    private String Middle;
    private LocalDate BirthDateTargetValue;

    public People() {
    }

    public People(String family, String name, String middle) {
        Family = family;
        Name = name;
        Middle = middle;
    }

    public People(String family, String name, String middle, LocalDate birthDateTargetValue) {
        Family = family;
        Name = name;
        Middle = middle;
        setBirthDateTargetValue(birthDateTargetValue);
    }

    public People(String family, String name, String middle, Date birthDateTargetValue) {
        Family = family;
        Name = name;
        Middle = middle;
        setBirthDateTargetValue(birthDateTargetValue);
    }

    public String getFamily() {
        return Family;
    }

    public void setFamily(String family) {
        Family = family;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMiddle() {
        return Middle;
    }

    public void setMiddle(String middle) {
        Middle = middle;
    }

    @Override
    public String toString() {
        return Family + " " + Name + " " + Middle;
    }

    public LocalDate getBirthDateTargetValue() {
        return BirthDateTargetValue;
    }

    public void setBirthDateTargetValue(Date birthData) {
        BirthDateTargetValue = birthData.toLocalDate();
    }

    public void setBirthDateTargetValue(LocalDate birthDateTargetValue) {
        BirthDateTargetValue = birthDateTargetValue;
    }
}
