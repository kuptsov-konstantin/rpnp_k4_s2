package models.employees;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import models.JDBCSQL;
import models.MainStage;
import models.administration.firm.Firm;
import models.administration.user.UserAddController;
import models.administration.user.UserEditController;
import models.param.UserTemp;

import java.net.URL;
import java.util.ResourceBundle;

public class EmployeesController extends MainStage implements Initializable {

    @FXML
    public Text employeeFirmTarget;
    @FXML
    public TableView employeeTable;
    @FXML
    public ListView firmstarget;
    @FXML
    public TableColumn employeeTableDepTarget;
    @FXML
    public TableColumn employeeTablePhoneTarget;
    @FXML
    public TableColumn employeeTableFirmTarget;
    @FXML
    public TableColumn employeeTableBirthDayTarget;
    @FXML
    public TableColumn employeeTableFIOTarget;
    ObservableList<Firm> olf = FXCollections.observableArrayList();
    ObservableList<EmployeeTable> olt = FXCollections.observableArrayList();


    public EmployeesController() {
        // super("/gui/Employees.fxml","Сотрудники", 600, 500);
        super.loadFxml(EmployeesController.class.getResource("/gui/Employees.fxml"), "Сотрудники", 715, 472, this, EmployeesController.class);
    }


    public void _init() {
        olf = JDBCSQL.getPersonFirms(UserTemp.getApplicationUserID());
        olf.add(0, new Firm("Все", -1));
        firmstarget.setItems(olf);
        firmstarget.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Firm>() {
                    public void changed(ObservableValue<? extends Firm> ov, Firm old_val, Firm new_val) {
                        if (new_val != null) {
                            employeeFirmTarget.setText("Сотрудники фирмы " + new_val.getNameFirm());
                            if (new_val.getIDFirm() != -1) {
                                olt.clear();
                                olt = JDBCSQL.getEmployees(new_val.getIDFirm());
                            } else {
                                ObservableList<Integer> mas = FXCollections.observableArrayList();
                                for (Firm firm : (ObservableList<Firm>) firmstarget.getItems()) {
                                    if (firm.getIDFirm() > -1) {
                                        mas.add(firm.getIDFirm());
                                    }
                                }
                                olt.clear();
                                olt = JDBCSQL.getEmployees(mas);

                            }
                            employeeTable.setItems(olt);
                        }
                    }
                });
        firmstarget.getSelectionModel().select(0);
        employeeTable.setItems(olt);
        ContextMenu menu = new ContextMenu();
        MenuItem izm_item = new MenuItem("Изменить");
        MenuItem del_item = new MenuItem("Удалить");
        // izm_item.setOnAction(event -> System.out.println("you've clicked the menu item"));
        izm_item.setOnAction(event -> {
            EmployeeTable person = (EmployeeTable) employeeTable.getSelectionModel().getSelectedItem();
            System.out.println(person.toString());
            UserEditController uec = new UserEditController(person.getAppUserID());
            uec.show();
        });
        del_item.setOnAction(event -> {
            EmployeeTable person = (EmployeeTable) employeeTable.getSelectionModel().getSelectedItem();
            JDBCSQL.JDBCUser.delUser(person.getAppUserID());

        });
        menu.getItems().addAll(izm_item, del_item);
        employeeTable.setContextMenu(menu);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        _init();
    }

    public void addNewEmployee(ActionEvent actionEvent) {
        UserAddController uac = new UserAddController();
        uac.show();
    }
}
