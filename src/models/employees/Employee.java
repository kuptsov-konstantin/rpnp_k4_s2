package models.employees;

/**
 * Created by Константин on 11.03.2016.
 */
public class Employee extends People {

    private boolean isChief;
    private String appUserId;

    public Employee() {
        super();
    }

    public Employee(String family, String name, String middle, boolean isChif, String appUserId) {
        super(family, name, middle);
        this.isChief = isChif;
        this.appUserId = appUserId;
    }

    public Employee(String nune) {
        super(nune, null, null);
        this.isChief = false;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public boolean isChif() {
        return isChief;
    }

    public void setChif(boolean chif) {
        isChief = chif;
    }

    public String getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(String appUserId) {
        this.appUserId = appUserId;
    }
}
