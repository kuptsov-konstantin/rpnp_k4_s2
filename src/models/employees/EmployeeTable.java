package models.employees;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EmployeeTable {
    private final StringProperty FIO;
    private final StringProperty BirthDay;
    private final StringProperty Phone;
    private final StringProperty Firm;
    private final StringProperty Dep;
    private String appUserID;

    public EmployeeTable() {
        this(null, null, null, null, null, null);
    }

    public EmployeeTable(String birthDay, String phone, String firm, String fio, String dep, String _appUserID) {
        this.FIO = new SimpleStringProperty(fio);
        this.BirthDay = new SimpleStringProperty(birthDay);
        this.Phone = new SimpleStringProperty(phone);
        this.Firm = new SimpleStringProperty(firm);
        this.Dep = new SimpleStringProperty(dep);
        this.appUserID = _appUserID;
    }

    public String getFIO() {
        return FIO.get();
    }

    public void setFIO(String fio) {
        this.FIO.set(fio);
    }

    public StringProperty FIOProperty() {
        return FIO;
    }

    public String getBirthDay() {
        return BirthDay.get();
    }

    public void setBirthDay(String birthDay) {
        this.BirthDay.set(birthDay);
    }

    public StringProperty birthDayProperty() {
        return BirthDay;
    }

    public String getPhone() {
        return Phone.get();
    }

    public void setPhone(String phone) {
        this.Phone.set(phone);
    }

    public StringProperty phoneProperty() {
        return Phone;
    }

    public String getFirm() {
        return Firm.get();
    }

    public void setFirm(String firm) {
        this.Firm.set(firm);
    }

    public StringProperty firmProperty() {
        return Firm;
    }

    public String getDep() {
        return Dep.get();
    }

    public void setDep(String dep) {
        this.Dep.set(dep);
    }

    public StringProperty depProperty() {
        return Dep;
    }

    public String getAppUserID() {
        return appUserID;
    }

    public void setAppUserID(String appUserID) {
        this.appUserID = appUserID;
    }

    @Override
    public String toString() {
        return getFIO();
    }


}
