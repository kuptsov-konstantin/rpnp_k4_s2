package models;

import javafx.beans.NamedArg;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

/**
 * Created by Константин on 06.03.2016.
 */
public class MainStage extends Stage {
    /**
     * Предыдущее окно
     */
    private Stage previousWindow = null;

    public MainStage() {
    }

    /**
     * @param fxmlPath       путь к fxml файлу
     * @param title          ниазвание окна
     * @param width          ширина
     * @param height         высота
     * @param rootController контроллер
     * @param clazz          класс
     */
    protected void loadFxml(URL fxmlPath, String title, @NamedArg("width") double width, @NamedArg("height") double height, Object rootController, Class<?> clazz) {

        FXMLLoader fxmlLoader = new FXMLLoader(fxmlPath) /*fxmlPath*/;
        // fxmlLoader.setController(this);
        if (clazz == rootController.getClass()) {
            fxmlLoader.setController(rootController);
        }
        //fxmlLoader.setRoot(rootController);
        try {
            Parent parent = (Parent) fxmlLoader.load();
            Scene scene = new Scene(parent, width, height);
            this.setScene(scene);
            this.setTitle(title);

            // this.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


       /* FXMLLoader fxmlLoader = new FXMLLoader(fxmlFile);
        if (clazz==rootController.getClass()){
           fxmlLoader.setController(rootController);
        }
        fxmlLoader.setRoot(rootController);

        try {
            Parent parent = (Parent) fxmlLoader.load();
            Scene scene = new Scene(parent, width, height);
            this.setScene(scene);
            this.setTitle(title);

            // this.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }*/
    }
  /*  public void  setController(Object obj){
        fxmlLoader.setController(obj);
    }*/


    /**
     * Закрытие окна
     */
    @Override
    public void close() {
        previousWindow.show();
        super.close();
    }


    public void nextStage() {
        super.hide();
    }

    /**
     * Возвращение  к предыдущему окну
     */
    @Override
    public void hide() {
        if (previousWindow != null) {
            previousWindow.show();
        }
        super.hide();
    }

    /**
     * @param previousWindow родительское окно
     */
    public void setPreviousWindow(Stage previousWindow) {
        this.previousWindow = previousWindow;
    }
}
