package models;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import models.administration.AdminsController;
import models.administration.firm.projects.ProjectsController;
import models.employees.EmployeesController;
import models.param.ParametersForVhod;
import models.param.UserTemp;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainWindowController extends MainStage implements Initializable {

    public Button adminButtonTarget;
    public Text logontarget;
    @FXML
    private Text actiontarget;


    /**
     * Конструктор главного окна
     */
    public MainWindowController() {
        //new FXMLLoader(MainWindowController.class.getResource("/gui/MainWindow.fxml"))
        //   super("/gui/MainWindow.fxml","Главное окно", 400, 500, this, MainWindowController.class);
        loadFxml(getClass().getResource("/gui/MainWindow.fxml"), "Главное окно", 400, 500, this, MainWindowController.class);

        // super.setController(this);
        MainWindowController.class.getResource("/gui/MainWindow.fxml");
    }

    /**
     * Загрузка окна администрирования
     *
     * @param actionEvent
     * @throws IOException
     */
    public void handleAdminButtonAction(ActionEvent actionEvent) throws IOException {
        AdminsController ac = new AdminsController();
        ac.setPreviousWindow(this);
        ac.show();
        this.nextStage();
    }

    /**
     * Загрузка списка пользователей
     *
     * @param actionEvent
     * @throws IOException
     */
    public void handleEmployeesButtonAction(ActionEvent actionEvent) throws IOException {
        EmployeesController EC = new EmployeesController();
        EC._init();
        EC.setPreviousWindow(this);
        EC.show();
        this.nextStage();

       /* Stage primaryStage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../GUI/Employees.fxml"));
        Parent root = (Parent)fxmlLoader.load();
        EmployeesController controller = fxmlLoader.<EmployeesController>getController();
        controller.setApplicationUserID(parametersPeredacha.getApplicationUserID());
        controller._init();
        primaryStage.setTitle("Сотрудники");
        primaryStage.setScene(new Scene(root, 600, 500));
        primaryStage.show();*/

    }

    /**
     * Загрузка списка проектов
     *
     * @param actionEvent
     * @throws IOException
     */
    public void handleProjectButtonAction(ActionEvent actionEvent) throws IOException {
        ProjectsController PC = new ProjectsController();
        PC.setPreviousWindow(this);
        PC.show();
        this.nextStage();

    }

    /**
     * Инициализатор окна
     */
    public void _init() {
        logontarget.setText("Добро пожаловать!\nУважаемый " + UserTemp.getUserName() + "!");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Выход
     *
     * @param actionEvent
     */
    public void onExitHandle(ActionEvent actionEvent) {
        ParametersForVhod.loginOut();
        this.hide();
        LoginController lg = new LoginController();
        lg.show();
    }
}
