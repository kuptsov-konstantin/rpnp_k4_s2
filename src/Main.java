import javafx.application.Application;
import javafx.stage.Stage;
import models.LoginController;
import models.MainWindowController;
import models.param.ParametersForVhod;
import models.param.ParametersPeredacha;
import models.param.UserTemp;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ParametersPeredacha pp = ParametersForVhod.loginRead();
        if (pp != null) {
            if (pp.isAutorise() == true) {
                //new UserTemp.UserInfo(pp.getApplicationUserID(),pp.getPasshashMySQL(),pp.getEmail(), pp.getUserName())
                UserTemp.userTempSave(pp);
                MainWindowController MWC = new MainWindowController();
                MWC._init();
                MWC.show();

               /* FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GUI/MainWindow.fxml"));
                Parent root = (Parent)fxmlLoader.load();
                MainWindowController controller = fxmlLoader.<MainWindowController>getController();
                controller.setParametersPeredacha(pp);
                controller._init();
                primaryStage.setTitle("Главное окно");
                primaryStage.setScene(new Scene(root, 400,500));
                primaryStage.show();*/
            } else {
                /*Parent root = FXMLLoader.load(getClass().getResource("GUI/login.fxml"));
                primaryStage.setTitle("Вход в базу");
                primaryStage.setScene(new Scene(root));
                primaryStage.show();*/
                LoginController LC = new LoginController();
                LC.show();
            }
        } else {
            LoginController LC = new LoginController();
            LC.show();
          /* Parent root = FXMLLoader.load(getClass().getResource("GUI/login.fxml"));
            primaryStage.setTitle("Вход в базу");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();*/
        }
    }

    @Override
    public void stop() throws Exception {
        UserTemp.userTempDelete();
        super.stop();
    }

}
